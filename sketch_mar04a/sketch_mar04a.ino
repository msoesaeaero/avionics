#define TEST_PIN 11
#define LED_PIN 4
#define IN_PIN 2

void setup() {
  // put your setup code here, to run once:
  pinMode(TEST_PIN, OUTPUT);
  pinMode(LED_PIN, OUTPUT);
  pinMode(IN_PIN, INPUT);
  digitalWrite(TEST_PIN, LOW);
  digitalWrite(LED_PIN, LOW);
}

void loop() {
  // put your main code here, to run repeatedly:

  if(digitalRead(IN_PIN) == HIGH)
  {
    digitalWrite(TEST_PIN, HIGH);
    digitalWrite(LED_PIN, HIGH);
    delay(1500);
    digitalWrite(TEST_PIN, LOW);
    digitalWrite(LED_PIN, LOW);
  }
}
