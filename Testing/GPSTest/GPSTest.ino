#include <SoftwareSerial.h>

//#define GPS_TEST
//#define SERIALPORT_TEST

//#ifdef GPS_TEST

#include <Wire.h>
#include <gps.h>
#include <gpsData.h>

void readFromGPS();

void setup()
{
  Serial.begin(38400);
  gps_init();
}

void loop()
{
  readFromGPS();
}

void readFromGPS()
{
  gps_data gpsData;

  Serial.println("---- Starting readFromGPS Test ----");
  
  gpsData = gps_getData();
  
  Serial.print("validity: ");
  Serial.print(gpsData.validity);
  Serial.print(" latitude degrees: ");
  Serial.print(gpsData.latitude.degrees);
  Serial.print(" latitude minutes: ");
  Serial.print(gpsData.latitude.minutes,5);
  Serial.print(" latitude indicator: ");
  Serial.print(gpsData.latitude.indicator);
  Serial.print(" longitude degrees: ");
  Serial.print(gpsData.longitude.degrees);
  Serial.print(" longitude minutes: ");
  Serial.print(gpsData.longitude.minutes,5);
  Serial.print(" longitude indicator: ");
  Serial.print(gpsData.longitude.indicator);
  Serial.print(" speed: ");
  Serial.print(gpsData.speed);
  Serial.print(" heading: ");
  Serial.println(gpsData.heading);
}
//#endif // #ifdef GPS_TEST

//#ifdef SERIALPORT_TEST
//#include <gpsData.h>
//
//SoftwareSerial serialPort(5, 4);
//
//void readFromSS();
//void readLikeGPS();
//
//void setup()
//{
//  Serial.begin(38400);
//  serialPort.begin(57600);
//}
//
//void loop()
//{
//  //readFromSS();
//  readLikeGPS();
//}
//
//void readFromSS()
//{
//  char ch = 0;
//  const long timeout = 1000;
//  long startTime;
//
//  Serial.println("---- Starting readFromSS Test ----");
//  
//  if(!serialPort.available())
//  {
//    Serial.println("Serial Port not available.");
//    return;
//  }
//
//  while(true)
//  {
//    startTime = millis();
//    while(!serialPort.available())
//    {
//      //long currTime = millis();
//      //Serial.println(currTime - startTime);
//      //Serial.println("Serial Port is not available.");
//      if((millis() - startTime) > timeout)
//      {
//        //Serial.println("Reading of Serial Port timed out (1 second).");
//        return;
//      }
//    }
//    //Serial.println('=');
//    ch = serialPort.read();
//    while(ch == -1)
//    {
//      ch = serialPort.read();
//    }
//    if(ch == '$')
//    {
//      //Serial.println("Wwwwowooaaaaooohwwwww!");
//    }
//    Serial.print(ch);
//  }
//  
//  Serial.println("Serial Port became unavailable.");
//}
//
//void readLikeGPS()
//{
//  int i = 0;
//  const long timeout = 250;
//  long startTime = 0;
//  char sentence[72];
//  gps_data data;
//  
//  // Wait for serial port to become available
//  startTime = millis();
//  while(!serialPort.available())
//  {
//    if((millis() - startTime) > timeout)
//    {
//      Serial.println("Read operation timed out.");
//      return; // GPS serial port took too long to become available
//    }
//  }
//
//  char ch = serialPort.read();
//  
//  // Get the start of a GPS string
//  while(ch == -1 || ch != '$')
//  {
//    ch = serialPort.read();
//  }
//
////  while(ch != '\n' && i < 72)
////  {
////    while(ch == -1)
////    {
////      ch = serialPort.read();
////    }
////
////    sentence[i] = ch;
////    i++;
////    ch = serialPort.read();
////  }
//  
//  // Capture the GPS string
//  while(ch != '\n' && i < 72)
//  {
//    if(ch != -1) // Character not available in SoftwareSerial buffer
//    {
//      sentence[i] = ch;
//      i++;
//    }
//    
//    ch = serialPort.read(); // Get following character
//  }
//  
//  sentence[i] = '\0';
//  Serial.println(sentence);
//  
//  // Parse the gps string into an easier to work with array
//  i = 0;
//  char* parsed[13];
//  parsed[i] = strtok(sentence, ",");
//  for (i = 0; parsed[i] != NULL; i++)
//  {
//    parsed[i+1] = strtok(NULL, ",");
//  }
//
//  // fill the gps_data struct to be returned
//  data.latitude.degrees = atoi(parsed[3])/100;
//  data.latitude.minutes = fmod(atof(parsed[3]),100);
//  if (strcmp(parsed[4], "N") == 0)
//    data.latitude.indicator = 0;
//  else if (strcmp(parsed[4], "S") == 0)
//    data.latitude.indicator = 1;
//  data.longitude.degrees = atoi(parsed[5])/100;
//  data.longitude.minutes = fmod(atof(parsed[5]),100);
//  if (strcmp(parsed[6], "E") == 0)
//    data.longitude.indicator = 0;
//  else if (strcmp(parsed[6], "W") == 0)
//    data.longitude.indicator = 1;
//  data.speed = atof(parsed[7])*1.68781;  // constant is conversion of knots to feet per second
//  data.heading = atof(parsed[8]);
//  if (strcmp(parsed[2], "V") == 0)
//    data.validity = 0;
//  else if (strcmp(parsed[2], "A") == 0)
//    data.validity = 1;
//
//  Serial.print("validity: ");
//  Serial.print(data.validity);
//  Serial.print(" latitude degrees: ");
//  Serial.print(data.latitude.degrees);
//  Serial.print(" latitude minutes: ");
//  Serial.print(data.latitude.minutes,5);
//  Serial.print(" latitude indicator: ");
//  Serial.print(data.latitude.indicator);
//  Serial.print(" longitude degrees: ");
//  Serial.print(data.longitude.degrees);
//  Serial.print(" longitude minutes: ");
//  Serial.print(data.longitude.minutes,5);
//  Serial.print(" longitude indicator: ");
//  Serial.print(data.longitude.indicator);
//  Serial.print(" speed: ");
//  Serial.print(data.speed);
//  Serial.print(" heading: ");
//  Serial.println(data.heading);
//}

//#endif //#ifdef SERIALPORT_TEST
