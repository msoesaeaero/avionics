#include <Wire.h>
#include <SoftwareSerial.h>
#include <alt.h>
#include <das.h>
#include <gpsData.h>
#include <comms.h>
#include <math.h>

extern "C"
{
  #include <tess.h>
  #include <tess_priv.h>
}

TESS tess;

// Prototypes
void testTESS();
void calcTESS();

// Macros

// Meters per degree of latitude @ given latitude 
#define TESS_MPLAT(lat) (111132.92 - 559.82*cos(2*(lat)) +\
                         1.175*cos(4*(lat)) - 0.0023*cos(6*(lat)))

// Meters per degree of longitude @ given latitude
#define TESS_MPLON(lat) (111412.84*cos((lat)) -\
                         93.5*cos(3*(lat)) - 0.118*cos(5*(lat)))

void setup()
{
  Serial.begin(38400);
  Serial.println("\r\n----TESS Unit Tests----");
  tess_init(&tess);
  //calcTESS();
  testTESS();
}

void loop()
{

}

void testTESS()
{
  gps_data demoPoints[18] = {{{43, 2.64194, 0}, {87, 54.40293, 1}, 30.26, 130.15},
                            {{43, 2.64033, 0}, {87, 54.39778, 1}, 30.34, 122.56},
                            {{43, 2.64010, 0}, {87, 54.39263, 1}, 30.42, 100.64},
                            {{43, 2.64010, 0}, {87, 54.38716, 1}, 30.44, 90.00},
                            {{43, 2.64127, 0}, {87, 54.38185, 1}, 30.48, 82.60},
                            {{43, 2.64292, 0}, {87, 54.37767, 1}, 30.55, 69.89},
                            {{43, 2.64551, 0}, {87, 54.37557, 1}, 30.62, 41.83},
                            {{43, 2.64868, 0}, {87, 54.37509, 1}, 30.67, 12.37},
                            {{43, 2.65127, 0}, {87, 54.37590, 1}, 30.77, 352.19},
                            {{43, 2.65198, 0}, {87, 54.37911, 1}, 30.85, 281.16},
                            {{43, 2.65210, 0}, {87, 54.38426, 1}, 30.88, 272.10},
                            {{43, 2.65257, 0}, {87, 54.38796, 1}, 30.91, 280.36},
                            {{43, 2.65480, 0}, {87, 54.39006, 1}, 30.99, 329.16},
                            {{43, 2.65727, 0}, {87, 54.39086, 1}, 31.08, 342.96},
                            {{43, 2.65986, 0}, {87, 54.39054, 1}, 31.16, 8.91},
                            {{43, 2.66139, 0}, {87, 54.38941, 1}, 31.21, 38.64},
                            {{43, 2.66292, 0}, {87, 54.38732, 1}, 31.20, 46.12},
                            {{43, 2.66304, 0}, {87, 54.38491, 1}, 31.16, 89.69}};

  float demoAltitude[18] = {100.25,
                            101.19,
                            101.98,
                            102.01,
                            103.26,
                            104.65,
                            106.19,
                            108.39,
                            109.73,
                            110.89,
                            111.20,
                            111.06,
                            110.56,
                            109.67,
                            109.16,
                            108.25,
                            107.43,
                            106.81};

  transStruct tStruct;
  int i = 0;
  DAS das;
  int dropped = 0;

  tess_setDAS(&tess, &das);
  tess_arm(&tess, demoPoints[17], 1);

  if(tess.numPayloads != 1)
  {
    Serial.println("Failed arming: numPayloads");
    return;
  }
  else if(tess.armed != 1)
  {
    Serial.println("Failed arming: armed signal");
    return;
  }
  else
  {
    Serial.println("\r\nTESS armed");
  }
  
  for(i = 0; i < 18; i++)
  {
    Serial.print("\r\ni: ");
    Serial.println(i);
    tess_sendData(&tess, demoPoints[i], demoAltitude[i]);
    tess_predict(&tess);
    Serial.print("TESS predictBuf dist = ");
    if(tess.predictBuf.index != 0)
    {
      Serial.println(tess.predictBuf.data[tess.predictBuf.index-1]);
      if(tess.predictBuf.index != 1)
      {
        Serial.println(tess.predictBuf.data[tess.predictBuf.index-2]);
      }
      else
      {
        Serial.println(tess.predictBuf.data[tess.predictBuf.length-1]);
      }
    }
    else
    {
      Serial.println(tess.predictBuf.data[tess.predictBuf.length-1]);
      Serial.println(tess.predictBuf.data[tess.predictBuf.length-2]);
    }

    if(das.dropped == 1)
    {
      Serial.println("DAS received drop signal");  
    }
    
    if(tess.armed == 0 && dropped == 0)
    {
      Serial.print("TESS disarmed: predicted a drop at i = ");
      Serial.println(i);
      dropped = 1;
    }
    tStruct.gpsData = demoPoints[i];
    tStruct.altitude = demoAltitude[i];
    tStruct.haveDropped = tess.armed;
    //comms_transmit(&tStruct);
    delay(100);
  }
}

void calcTESS()
{
  float     vx;   // velocity - x axis
  float     vz;   // velocity - z axis
  float     pgx;  // Longitude of current position
  float     pgz;  // Latitude of current position
  float     lgx;  // Longitude of landing location
  float     lgz;  // Latitude of landing location
  float     tgx;  // Longitude of target location
  float     tgz;  // Latitude of target location
  float     dist; // Distance from landing to target in feet
  gps_data  gps;  // Most recent GPS data
  float     elevation;      // Most recent elvation reading
  
  int i = 0;
  
  const gps_data demoPoints[18] = {{{43, 2.64194, 0}, {87, 54.40293, 1}, 30.26, 130.15},
                            {{43, 2.64033, 0}, {87, 54.39778, 1}, 30.34, 122.56},
                            {{43, 2.64010, 0}, {87, 54.39263, 1}, 30.42, 100.64},
                            {{43, 2.64010, 0}, {87, 54.38716, 1}, 30.44, 90.00},
                            {{43, 2.64127, 0}, {87, 54.38185, 1}, 30.48, 82.60},
                            {{43, 2.64292, 0}, {87, 54.37767, 1}, 30.55, 69.89},
                            {{43, 2.64551, 0}, {87, 54.37557, 1}, 30.62, 41.83},
                            {{43, 2.64868, 0}, {87, 54.37509, 1}, 30.67, 12.37},
                            {{43, 2.65127, 0}, {87, 54.37590, 1}, 30.77, 352.19},
                            {{43, 2.65198, 0}, {87, 54.37911, 1}, 30.85, 281.16},
                            {{43, 2.65210, 0}, {87, 54.38426, 1}, 30.88, 272.10},
                            {{43, 2.65257, 0}, {87, 54.38796, 1}, 30.91, 280.36},
                            {{43, 2.65480, 0}, {87, 54.39006, 1}, 30.99, 329.16},
                            {{43, 2.65727, 0}, {87, 54.39086, 1}, 31.08, 342.96},
                            {{43, 2.65986, 0}, {87, 54.39054, 1}, 31.16, 8.91},
                            {{43, 2.66139, 0}, {87, 54.38941, 1}, 31.21, 38.64},
                            {{43, 2.66292, 0}, {87, 54.38732, 1}, 31.20, 46.12},
                            {{43, 2.66304, 0}, {87, 54.38491, 1}, 31.16, 89.69}};

  float demoAltitude[18] = {100.25,
                            101.19,
                            101.98,
                            102.01,
                            103.26,
                            104.65,
                            106.19,
                            108.39,
                            109.73,
                            110.89,
                            111.20,
                            111.06,
                            110.56,
                            109.67,
                            109.16,
                            108.25,
                            107.43,
                            106.81};

  Serial.println("--Starting calcTESS Test--");

  //return;
  tess.target = demoPoints[17];
  //tess.target = demoPoints[17];
  
  for(i = 0; i < 18; i++)
  {
    Serial.println("----------------------");
    Serial.println(i);

    gps = demoPoints[i];
    elevation = demoAltitude[i];
    
    // Determine vectorized velocity
    vx = gps.speed * sin(gps.heading * M_PI / 180.0);
    vz = gps.speed * cos(gps.heading * M_PI / 180.0);

    Serial.print("vx = ");
    Serial.println(vx, 6);
    Serial.print("vx = ");
    Serial.println(vz, 6);
    
    // Determine current position coordinates
    pgx = ((float)gps.longitude.degrees + gps.longitude.minutes/60.0) * pow(-1.0, gps.longitude.indicator);
    pgz = ((float)gps.latitude.degrees + gps.latitude.minutes/60.0) * pow(-1.0, gps.latitude.indicator);

    Serial.print("pgx = ");
    Serial.println(pgx, 6);
    Serial.print("pgz = ");
    Serial.println(pgz, 6);
    
    // Determine target location coordinates
    tgx = ((float)tess.target.longitude.degrees + tess.target.longitude.minutes/60.0) * pow(-1.0, tess.target.longitude.indicator);
    tgz = ((float)tess.target.latitude.degrees + tess.target.latitude.minutes/60.0) * pow(-1.0, tess.target.latitude.indicator);

    Serial.print("tgx = ");
    Serial.println(tgx, 6);
    Serial.print("tgz = ");
    Serial.println(tgz, 6);
    
    // Determine landing coordinates
    //  p + v * t
    //  0.249222 t/(ft)^(1/2) = sqrt(2/a)
    //  30.2808399 ft/m
    //  v is ft/s, elevation is feet, p is degrees (lat/lng)
    lgx = pgx + vx * 0.249222 * sqrt(elevation) / 3.2808399 / TESS_MPLON(pgz);
    lgz = pgz + vz * 0.249222 * sqrt(elevation) / 3.2808399 / TESS_MPLAT(pgz);

    Serial.print("lgx = ");
    Serial.println(lgx, 6);
    Serial.print("lgz = ");
    Serial.println(lgz, 6);
    
    // Determine distance from landing to target (in feet)
    dist = 3.2808399 * sqrt(pow((tgx - lgx) * TESS_MPLON((tgz+lgz)/2.0), 2) + pow((tgz - lgz) * TESS_MPLAT((tgz+lgz)/2.0), 2));
  
    Serial.print("dist = ");
    Serial.println(dist);
    // Check to see if distance has broken higherThreshold.
    // If so, drop payload
    if(dist <= tess.higherThreshold)
    {
      Serial.println("Within higher threshold. DROP!");
    }
    // If distance is between higher and lower thresholds, then check previous valid measurement.
    else if(dist <= tess.lowerThreshold)
    {
      Serial.println("Within lower threshold but not the higher threshold.");
    }
  }
}

