#include <Wire.h>
#include <stdint.h>

#define BASE_SAMPLES 10
#define SAMPLE_SIZE 50
#define ALTIMETER_ADDRESS 0x76
#define RESET 0x1E
#define ADC_READ 0x00
#define ADC_CONV 0x40
#define ADC_D1 0x00
#define ADC_D2 0x10
#define ADC_256 0x00
#define ADC_512 0x02
#define ADC_1024 0x04
#define ADC_2048 0x06
#define ADC_4096 0x08
#define PROM_READ 0xA0

#define SENS 0
#define OFF 1
#define TCS 2
#define TCO 3
#define TREF 4
#define TEMPSENS 5

const double P0 = 101570;
const double T0 = 288.15;
const double TEMP_GRADIENT = .0065;
const double R = 287.05;
const double g = 9.807;

void reset()
{
  Wire.begin();
  Wire.beginTransmission(ALTIMETER_ADDRESS);
  Wire.write(RESET);
  Wire.endTransmission();
  delay(3);
}

void readCoefficients()
{
  int i;
  for(i = 0; i < 6; i++)
  {
    uint16_t coefficient = 0;
    Wire.beginTransmission(ALTIMETER_ADDRESS);
    Wire.write(PROM_READ + (i+1)*2);
    Wire.endTransmission();

    Wire.requestFrom(ALTIMETER_ADDRESS, 2);
    if(Wire.available() >= 2)
    {
      coefficient = Wire.read();
      coefficient = coefficient << 8;
      coefficient = coefficient + Wire.read();
    }
    coefficients[i] = coefficient;
  }
}

long adcRead(int sensor)
{
  // tell the sensor to begin a conversion
  Wire.beginTransmission(ALTIMETER_ADDRESS);
  Wire.write(ADC_CONV | sensor);
  Wire.endTransmission();

  // ensure the conversion is finished before reading
  delay(10);

  // send the read command to the sensor
  Wire.beginTransmission(ALTIMETER_ADDRESS);
  Wire.write(ADC_READ);
  Wire.endTransmission();

  // request data from the sensor and return the read value
  Wire.requestFrom(ALTIMETER_ADDRESS, 3);
  if(Wire.available() >= 3)
  {
    long value = Wire.read();
    value = value << 8;
    value = value + Wire.read();
    value = value << 8;
    value = value + Wire.read();

    return value;
  }

  return '\n';
}

long calculatePressure()
{
  // digital pressure value
  unsigned long D1 = adcRead(ADC_D1 | ADC_4096);
  // digital temperature value
  unsigned long D2 = adcRead(ADC_D2 | ADC_4096);
  
  const long dT = D2 - ((unsigned long)coefficients[TREF] << 8);
  long TEMP = 2000 + (dT * (double)coefficients[TEMPSENS] / pow(2,23));
  

  long offset = (unsigned long)coefficients[OFF] * 4 + (((double)coefficients[TCO] / 1024) * ((double)dT / 2048));
  long sensitivity = (unsigned long)coefficients[SENS] * 2 + (((double)coefficients[TCS] / 1024) * ((double)dT / 4096));

  // these equations won't work without changes because of the modified offset and sensitivity equations
  long T2;
  long OFF2;
  long SENS2;
  if(TEMP < 2000)
  {
    T2 = pow((double)dT,2) / pow(2,31);
    Serial.println(T2);
    OFF2 = 61 * pow((double)TEMP-2000,2) / pow(2,4);
    SENS2 = 2 * pow(TEMP-2000,2);
  }
  else
  {
    T2 = 0;
    OFF2 = 0;
    SENS2 = 0;
  }
  if(TEMP < -1500)
  {
    OFF2 = OFF2 + 15 * pow(TEMP+1500,2);
    SENS2 = SENS2 + 8 * pow(TEMP+1500,2);
  }
  
  TEMP -= (T2/pow(2,15));
  offset -= (OFF2/pow(2,15));
  sensitivity -= (SENS2/pow(2,15));

  long compensatedPressure = ((double)D1 / 2048) * ((double)sensitivity / 1024) - offset;

//Serial.print("D2: ");
//  Serial.println(D2);
//  Serial.print("dT: ");
//  Serial.println(dT);
//  Serial.print("actual temp: ");
//  Serial.println(TEMP);
//    Serial.print("OFF: ");
//  Serial.println(offset);
//  Serial.print("SENS: ");
//  Serial.println(sensitivity);
//  Serial.print("pressure: ");
//  Serial.println(compensatedPressure);

  return compensatedPressure;
}

double getAltitude_ft(long compensatedPressure)
{
  double altitude = (T0 / TEMP_GRADIENT) * (1 - pow((compensatedPressure / P0),(TEMP_GRADIENT*(R/g))));
  altitude = altitude * 3.28084;
  return altitude;
}

double getAvgAltitude_ft()
{
  double avgAltitude = 0;
  int i;
  for(i = 0; i < SAMPLE_SIZE; i++)
  {
    avgAltitude += getAltitude_ft(calculatePressure());
  }
  return avgAltitude/SAMPLE_SIZE;
}

double getStartingAltitude()
{
  double baseHeight = 0;
  int i = 0;
  for(i = 0; i < BASE_SAMPLES; i++)
  {
    baseHeight += getAltitude_ft(calculatePressure());
  }

  baseHeight = baseHeight/BASE_SAMPLES;

  return baseHeight;
}

