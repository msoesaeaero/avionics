#define ADC_D1 0x00
#define ADC_D2 0x10
#define ADC_4096 0x08


uint16_t coefficients[6];
double baseHeight;

// A4 for SDA
// A5 for SCL
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  reset();
  readCoefficients();
  baseHeight = getStartingAltitude();
}

void loop() {
  // put your main code here, to run repeatedly:
  static double minimum = 10000;
  static double maximum = 0;
  double altitude = getAvgAltitude_ft();
  if(altitude < minimum)
  {
    minimum = altitude;
  }
  if(altitude > maximum)
  {
    maximum = altitude;
  }
  Serial.println(maximum - minimum);
//  Serial.print("altitude: ");
//  Serial.println(getAvgAltitude_ft());
}
