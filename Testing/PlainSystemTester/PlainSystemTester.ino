#include <Wire.h>
#include <SoftwareSerial.h>
#include <comms.h>
#include <gps.h>
#include <gpsData.h>
#include <alt.h>

#define NOTCON_PIN  8
#define FLIGHTU_PIN 9
#define FLIGHTA_PIN 10

#define DROP_A_PIN 11
#define DROP_B_PIN 12

bool haveDropped = 0;
gps_data demoPoints[18] = {{{43, 2.64194, 0}, {87, 54.40293, 1}, 30.26, 130.15},
                          {{43, 2.64033, 0}, {87, 54.39778, 1}, 30.34, 122.56},
                          {{43, 2.64010, 0}, {87, 54.39263, 1}, 30.42, 100.64},
                          {{43, 2.64010, 0}, {87, 54.38716, 1}, 30.44, 90.00},
                          {{43, 2.64127, 0}, {87, 54.38185, 1}, 30.48, 82.60},
                          {{43, 2.64292, 0}, {87, 54.37767, 1}, 30.55, 69.89},
                          {{43, 2.64551, 0}, {87, 54.37557, 1}, 30.62, 41.83},
                          {{43, 2.64868, 0}, {87, 54.37509, 1}, 30.67, 12.37},
                          {{43, 2.65127, 0}, {87, 54.37590, 1}, 30.77, 352.19},
                          {{43, 2.65198, 0}, {87, 54.37911, 1}, 30.85, 281.16},
                          {{43, 2.65210, 0}, {87, 54.38426, 1}, 30.88, 272.10},
                          {{43, 2.65257, 0}, {87, 54.38796, 1}, 30.91, 280.36},
                          {{43, 2.65480, 0}, {87, 54.39006, 1}, 30.99, 329.16},
                          {{43, 2.65727, 0}, {87, 54.39086, 1}, 31.08, 342.96},
                          {{43, 2.65986, 0}, {87, 54.39054, 1}, 31.16, 8.91},
                          {{43, 2.66139, 0}, {87, 54.38941, 1}, 31.21, 38.64},
                          {{43, 2.66292, 0}, {87, 54.38732, 1}, 31.20, 46.12},
                          {{43, 2.66304, 0}, {87, 54.38491, 1}, 31.16, 89.69}};

float demoAltitude[18] = {100.25,
                          101.19,
                          101.98,
                          102.01,
                          103.26,
                          104.65,
                          106.19,
                          108.39,
                          109.73,
                          110.89,
                          111.20,
                          111.06,
                          110.56,
                          109.67,
                          109.16,
                          108.25,
                          107.43,
                          106.81};
void demo();
void dropOnArm();

void setup()
{
  Serial.begin(38400);
  pinMode(DROP_A_PIN, OUTPUT);
  pinMode(DROP_B_PIN, OUTPUT);
  pinMode(NOTCON_PIN, OUTPUT);
  pinMode(FLIGHTU_PIN, OUTPUT);
  pinMode(FLIGHTA_PIN, OUTPUT);
  transStruct tStruct;
  recStruct rStruct;

  //Altimeter_init();
  tStruct.altitude = 0;
  tStruct.haveDropped = 0;
  digitalWrite(NOTCON_PIN, HIGH);
  digitalWrite(FLIGHTU_PIN, LOW);
  
  while(!comms_receive(&rStruct));

  comms_transmit(&tStruct);

  while(!comms_receive(&rStruct));
  
  digitalWrite(NOTCON_PIN, LOW);
  digitalWrite(FLIGHTU_PIN, HIGH);

  // Demo (blocking call, i.e. loop() will not be called afterwards)
  //demo();

  // Drop on Arm
  dropOnArm();
}

void loop()
{
  transStruct tStruct;
  recStruct rStruct;

  tStruct.gpsData = gps_getData();
  tStruct.altitude = Altimeter_getAvgAltitude_ft();
  tStruct.haveDropped = 0;

  digitalWrite(FLIGHTA_PIN, HIGH);
  comms_transmit(&tStruct);
  digitalWrite(FLIGHTA_PIN, LOW);
  
  delay(50);
//  if(comms_receive(&rStruct))
//  {
//    if(rStruct.manDrop > 0)
//    {
//      if(!haveDropped)
//      {
//        digitalWrite(DROP_A_PIN, HIGH);
//        delay(500);
//        digitalWrite(DROP_A_PIN, LOW);
//  
//        tStruct.haveDropped = 1;
//        haveDropped = 1;
//      }
//
//      rStruct.manDrop = 0;
//    }
//  }
}

void demo()
{
  transStruct tStruct;
  recStruct rStruct;
  int i;

  for(i = 0; i < 18; i++)
  {
    // Check for a received packet first
//    if(comms_receive(&rStruct))
//    {
//      if(rStruct.manDrop > 0)
//      {
//        if(!haveDropped)
//        {
//          digitalWrite(DROP_A_PIN, HIGH);
//          delay(500);
//          digitalWrite(DROP_A_PIN, LOW);
//    
//          tStruct.haveDropped = 1;
//          haveDropped = 1;
//        }
//        else
//        {
//          tStruct.haveDropped = 0;
//        }
//  
//        rStruct.manDrop = 0;
//      }
//      
//      if(rStruct.armed)
//      {
//        digitalWrite(FLIGHTU_PIN, LOW);
//        digitalWrite(FLIGHTA_PIN, HIGH);      
//      }
//      else
//      {
//        digitalWrite(FLIGHTA_PIN, LOW);
//        digitalWrite(FLIGHTU_PIN, HIGH);
//      }
//    }
//    Serial.print("i: ");
//    Serial.print(i);
//    Serial.print("| Lat: ");
//    Serial.print(demoPoints[i].latitude.degrees);
//    Serial.print(", ");
//    Serial.print(demoPoints[i].latitude.minutes);
//    Serial.print(", ");
//    Serial.println(demoPoints[i].latitude.indicator);
//    Serial.print(" Lng: ");
//    Serial.print(demoPoints[i].longitude.degrees);
//    Serial.print(", ");
//    Serial.print(demoPoints[i].longitude.minutes);
//    Serial.print(", ");
//    Serial.println(demoPoints[i].longitude.indicator);
//    Serial.print(" Speed: ");
//    Serial.println(demoPoints[i].speed);
//    Serial.print(" Heading: ");
//    Serial.println(demoPoints[i].heading);
    
    tStruct.gpsData.latitude.degrees = demoPoints[i].latitude.degrees;
    tStruct.gpsData.latitude.minutes = demoPoints[i].latitude.minutes;
    tStruct.gpsData.latitude.indicator = demoPoints[i].latitude.indicator;
    tStruct.gpsData.longitude.degrees = demoPoints[i].longitude.degrees;
    tStruct.gpsData.longitude.degrees = demoPoints[i].longitude.degrees;
    tStruct.gpsData.longitude.degrees = demoPoints[i].longitude.degrees;
    tStruct.gpsData.speed = demoPoints[i].speed;
    tStruct.gpsData.heading = demoPoints[i].heading;
    tStruct.altitude = demoAltitude[i];

    comms_transmit(&tStruct);
    delay(500);
  }

  while(1)
  {
    if(comms_receive(&rStruct))
    {
      if(rStruct.manDrop > 0)
      {
        if(!haveDropped)
        {
          digitalWrite(DROP_A_PIN, HIGH);
          delay(500);
          digitalWrite(DROP_A_PIN, LOW);
        }
  
        rStruct.manDrop = 0;
      }
      
      if(rStruct.armed)
      {
        digitalWrite(FLIGHTU_PIN, LOW);
        digitalWrite(FLIGHTA_PIN, HIGH);      
      }
      else
      {
        digitalWrite(FLIGHTA_PIN, LOW);
        digitalWrite(FLIGHTU_PIN, HIGH);
      }
    }
  }
}

void dropOnArm()
{
  transStruct tx;
  recStruct rx;
  int skipRx = 0;
  //float startAlt = Altimeter_getAltitude_ft(Altimeter_calculatePressure());
  
  tx.altitude = 123.654;
  tx.haveDropped = 0;
  
  while(1)
  {
    //tx.altitude = Altimeter_getAltitude_ft(Altimeter_calculatePressure()) - startAlt;
    comms_transmit(&tx);
    tx.haveDropped = 0;
    
    while(!comms_receive(&rx)); // Wait to receive a packet

    if(!skipRx && rx.armed)
    {
      digitalWrite(DROP_A_PIN, HIGH);
      digitalWrite(DROP_B_PIN, HIGH);
      delay(500);
      digitalWrite(DROP_A_PIN, LOW);
      digitalWrite(DROP_B_PIN, LOW);

      tx.haveDropped = 1;
      skipRx = 11;
    }
    else
    {
      delay(50);
    }

    if(skipRx > 0)
    {
      skipRx--;
    }
  }
}

