int _count = 0;
int _count2 = 0;

void setup()
{
  Serial.begin(9800);
  Serial.write("Setting up timer.\n");
  
  // initialize timer1
  noInterrupts();
 
  // Timer1
  TCCR1A = 0;
  TCCR1B = 0;
  TCNT1  = 0;
  OCR1A = 3125;             // compare register 16MHz/256/20Hz
  TCCR1B |= (1 << WGM12);   // enable CTC mode
  TCCR1B |= (1 << CS12);    // use the 256 clock prescaler
  TIMSK1 |= (1 << OCIE1A);  // enable timer compare interrupt

  // Timer2
  TCCR2A = 0;
  TCCR2B = 0;
  TCNT2  = 0;
  OCR2A = 250;              // compare register 16MHz/256/250Hz
  TCCR2B |= (1 << WGM12);   // enable CTC mode
  TCCR2B |= (1 << CS12);    // use the 256 clock prescaler
  TIMSK2 |= (1 << OCIE1A);  // enable timer compare interrupt
  interrupts();
  
  Serial.write("Timer setup completed.\n");
}

void loop()
{
  while(1)
  {
    Serial.write("-------------------------\n");
    delay(1000);
  }
}

ISR(TIMER1_COMPA_vect)
{
  Serial.write("Count = ");
  Serial.print(_count++);
  Serial.write("\n");
}

ISR(TIMER2_COMPA_vect)
{
  if(++_count2 >= 250) // Two seconds
  {
    Serial.write("HELLO WORLD!!!\n");
    _count2 = 0;
  }
}

