#define BTN_PIN 2
#define OUT_PIN 4

void setup()
{
  pinMode(BTN_PIN, INPUT);
  pinMode(OUT_PIN, OUTPUT);
  digitalWrite(OUT_PIN, LOW);
}

void loop()
{
  if(digitalRead(BTN_PIN) == HIGH)
  {
    digitalWrite(OUT_PIN, HIGH);
  }
  else
  {
    digitalWrite(OUT_PIN, LOW);
  }
}
