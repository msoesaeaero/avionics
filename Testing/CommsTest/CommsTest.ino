/*****************************************************************
CommsTest.ino

Set up a software serial port to pass data between an XBee Shield
and the serial monitor.

Hardware Hookup:
  The XBee Shield makes all of the connections you'll need
  between Arduino and XBee. If you have the shield make
  sure the SWITCH IS IN THE "DLINE" POSITION. That will connect
  the XBee's DOUT and DIN pins to Arduino pins 2 and 3.

*****************************************************************/
// We'll use SoftwareSerial to communicate with the XBee:
#include <SoftwareSerial.h>
#include <Wire.h>
#include <gpsData.h>
#include <comms.h>
#include <gps.h>
#include <alt.h>
// XBee's DOUT (TX) is connected to pin 1 (Arduino's Software RX)
// XBee's DIN (RX) is connected to pin 0 (Arduino's Software TX)

#define SENTENCESIZE 72
#define TIME 1
#define VALIDITY 2
#define LATITUDE 3
#define LATITUDE_HEMISPHERE 4
#define LONGITUDE 5
#define LONGITUDE_HEMISPHERE 6
#define SPEED 7
#define HEADING 8
#define DATE 9
#define MAGNETIC_VARIATION 10
#define CHECKSUM 11

int commsTest_simpleTransmit();
void comms_getData();
gps_data mock_gps_getData();

//SoftwareSerial mockgpsSerial(5, 4);

void setup()
{
  // Set up both ports at 9600 baud. This value is most important
  // for the XBee. Make sure the baud rate matches the config
  // setting of your XBee.

  Serial.begin(38400);
  gps_init();
  comms_init();
  //mockgpsSerial.begin(57600);
  
  //Serial.write("\n\r---- Begin commsTest_tx Test ----\n\r");
  //if(commsTest_tx())
  //{
  //  Serial.write("commsTest_tx test passed.\n\r");
  //}
  //else
  //{
  //  Serial.write("commsTest_tx test failed.\n\r");
  //}
  //Serial.write("\n\r---- Begin commsTest_transmit Test ----\n\r");
  //if(commsTest_transmit())
  //{
  //  Serial.write("\n\rcommsTest_transmit test passed.\n\r");
  //}
  //else
  //{
  //  Serial.write("\n\rcommsTest_transmit test failed.\n\r");
  //}
  //commsTest_transmit();
  //commsTest_transmitMass();
  Altimeter_init();
  //commsTest_tx();
  //commsTest_simpleTransmit();
}

void loop()
{
//  if(Serial.available() >= sizeof(recStruct))
//  {
//    Serial.print("\n\r---- Begin commsTest_receive Test ----\n\r");
//    if(commsTest_receive() == 1)
//    {
//      Serial.print("\n\rcommsTest_receive test passed.\n\r");
//    }
//    else
//    {
//      Serial.print("\n\rcommsTest_receive test failed.\n\r");
//    }
////    commsTest_rx();
//  }
	//delay(1000);
	comms_getData();
	delayMicroseconds(100000);
}

// Tests transmitting data
// Sends "Hello World!" over the tx output
// Returns '1' if successful, '0' if failed
int commsTest_tx()
{
  int passed = 1;
  Serial.write("Hello World!\n\r");
  return passed;
}
int commsTest_transmit()
{
  int passed = 1;
  transStruct data;
  gps_data gpsdata;

  // Set up transStruct to send
  gpsdata.latitude.degrees = 60;
  gpsdata.latitude.minutes = 25.600932;
  gpsdata.latitude.indicator = 0;
  gpsdata.longitude.degrees = 50;
  gpsdata.longitude.minutes = 35.608832;
  gpsdata.longitude.indicator = 1;
  gpsdata.speed = 32.42;
  gpsdata.heading = 271.113;
  data.gpsData = gpsdata;
  data.haveDropped = 1;
  data.altitude = 104.67;
  data.header = 0x73;

  // Send transStruct
  comms_transmit(&data);
  return passed;
}

int commsTest_transmitMass()
{
  int passed = 1;
  transStruct packetList[40];
  int i;

  // Set up the seed packet
//  packetList[0].gpsData.latitude.degrees = 32;
//  packetList[0].gpsData.latitude.minutes = 36.58326;
  packetList[0].gpsData.latitude.degrees = 43;
  packetList[0].gpsData.latitude.minutes = 2.630050;
  packetList[0].gpsData.latitude.indicator = 0;
//  packetList[0].gpsData.longitude.degrees = 97;
//  packetList[0].gpsData.longitude.minutes = 29.01474;
  packetList[0].gpsData.longitude.degrees = 87;
  packetList[0].gpsData.longitude.minutes = 54.36691;
  packetList[0].gpsData.longitude.indicator = 1;
  packetList[0].gpsData.speed = 32.42;
  packetList[0].gpsData.heading = 271.113;
  packetList[0].haveDropped = 0;
  packetList[0].altitude = 104.67;

  // Construct the remaining packets in the list
  constructPackets(packetList, 40);

  // Send the messages from the packet list at a 20 Hz rate
  for(i = 0; i < 35; i++)
  {
    comms_transmit(&(packetList[i]));
    delay(250); // 20 Hz = 1 packet / 50 ms
  }
}

int commsTest_simpleTransmit()
{
  int passed = 1;
  transStruct data;
  gps_data gpsdata;

  // Set up transStruct to send
  gpsdata.latitude.degrees = 0x12;
  gpsdata.latitude.minutes = 0x34567890;
  gpsdata.latitude.indicator = 0x78;
  gpsdata.longitude.degrees = 0x90;
  gpsdata.longitude.minutes = 0x12345678;
  gpsdata.longitude.indicator = 0x56;
  gpsdata.speed = 0x78901234;
  gpsdata.heading = 0x56789012;
  gpsdata.validity = 0x23;
  data.gpsData = gpsdata;
  data.haveDropped = 0x34;
  data.altitude = 100.1;
  data.header = 0x73;

  // Send transStruct
  while(true)
  {
    comms_transmit(&data);
    data.altitude = Altimeter_getAvgAltitude_ft();
    delay(100);
  }
  return passed;
}

void commsTest_rx()
{
  struct recStruct* recData;//create pointer to data
  int dataSize = sizeof(recStruct);
  char input[dataSize];
  
  // Exit if null parameter or serial data is 
  //  not ready to be received
  if((unsigned)(Serial.available()) < sizeof(recStruct))
  {
    Serial.println("At size check");
    return;
  }
  
  // Read the receive data into the input stream
  Serial.readBytes(input, dataSize);
  
  // Unpack data buffer into a recStruct
  unsigned char* dataPointer = (unsigned char*) input;
  
  recData->armed = *(dataPointer);
  Serial.println(*(dataPointer));
  dataPointer++;
  recData->manDrop = *(dataPointer);
  Serial.println(*(dataPointer));
  dataPointer++;
  recData->headwind = *(dataPointer);
  Serial.println(*(dataPointer));
  dataPointer++;
  recData->windspeed = *((float*)dataPointer);
  Serial.println(*(dataPointer));
  dataPointer++;
  Serial.println(*(dataPointer));
  dataPointer++;
  Serial.println(*(dataPointer));
  dataPointer++;
  Serial.println(*(dataPointer));
  dataPointer++;
  recData->packetsToDrop = *((int*)dataPointer);
  Serial.println(*(dataPointer));
  dataPointer++;
  Serial.println(*(dataPointer));
  dataPointer++;
  recData->changeDropLocation = *((unsigned char*)dataPointer);
  Serial.println(*(dataPointer));
  dataPointer++;
  recData->newLong.degrees = *((unsigned char*)dataPointer);
  Serial.println(*(dataPointer));
  dataPointer++;
  recData->newLong.minutes = *((float*)dataPointer);
  Serial.println(*(dataPointer));
  dataPointer++;
  Serial.println(*(dataPointer));
  dataPointer++;
  Serial.println(*(dataPointer));
  dataPointer++;
  Serial.println(*(dataPointer));
  dataPointer++;
  recData->newLong.indicator = *((unsigned char*)dataPointer);
  Serial.println(*(dataPointer));
  dataPointer++;
  recData->newLat.degrees = *((unsigned char*)dataPointer);
  Serial.println(*(dataPointer));
  dataPointer++;
  recData->newLat.minutes = *((float*)dataPointer);
  Serial.println(*(dataPointer));
  dataPointer++;
  Serial.println(*(dataPointer));
  dataPointer++;
  Serial.println(*(dataPointer));
  dataPointer++;
  Serial.println(*(dataPointer));
  dataPointer++;
  recData->newLat.indicator = *((unsigned char*)dataPointer);
  Serial.println(*(dataPointer));
  dataPointer++;
  recData->calAltimeter = *((unsigned char*)dataPointer);
  Serial.println(*(dataPointer));
  Serial.println("------------------------------");
  Serial.print("Armed: ");
  Serial.print(recData->armed);
  Serial.print(" calAltimeter: ");
  Serial.print(recData->calAltimeter);
  Serial.print(" changeDropLocation: ");
  Serial.print(recData->changeDropLocation);
  Serial.print(" headwind: ");
  Serial.print(recData->headwind);
  Serial.print(" manDrop: ");
  Serial.println(recData->manDrop);
  Serial.print("newLat.degrees: ");
  Serial.print(recData->newLat.degrees);
  Serial.print(" newLat.minutes: ");
  Serial.print(recData->newLat.minutes, 6);
  Serial.print(" newLat.indicator: ");
  Serial.println(recData->newLat.indicator);
  Serial.print("newLong.degrees: ");
  Serial.print(recData->newLong.degrees);
  Serial.print(" newLong.minutes: ");
  Serial.print(recData->newLong.minutes, 6);
  Serial.print(" newLong.indicator: ");
  Serial.println(recData->newLong.indicator);
  Serial.print("packetsToDrop: ");
  Serial.print(recData->packetsToDrop);
  Serial.print(" windspeed: ");
  Serial.println(recData->windspeed, 2);
  Serial.println("------------------------------");
}

int commsTest_receive()
{
  int passed = 1;
  recStruct data;
  recStruct expected;

  // Initialize data struct
  data.armed = 0;
  expected.calAltimeter = 0;
  expected.changeDropLocation = 0;
  expected.headwind = 0;
  expected.manDrop = 0;
  expected.newLat.degrees = 0;
  expected.newLat.minutes = 0.0;
  expected.newLat.indicator = 0;
  expected.newLong.degrees = 0;
  expected.newLong.minutes = 0.0;
  expected.newLong.indicator = 0;
  expected.packetsToDrop = 0;
  expected.windspeed = 0;
  
  // The expected recStruct to receive
  expected.armed = 1;
  expected.calAltimeter = 1;
  expected.changeDropLocation = 0;
  expected.headwind = 1;
  expected.manDrop = 0;
  expected.newLat.degrees = 60;
  expected.newLat.minutes = 25.600932;
  expected.newLat.indicator = 0;
  expected.newLong.degrees = 50;
  expected.newLong.minutes = 35.608832;
  expected.newLong.indicator = 1;
  expected.packetsToDrop = 2;
  expected.windspeed = 32.42;
  
  if(comms_receive(&data) == 0)
  {
    passed = 0;
    Serial.println("Failed receive.");
    return passed;
  }
  else
  {
    Serial.println("------------------------------");
    Serial.print("Armed: ");
    Serial.print(data.armed);
    Serial.print(" calAltimeter: ");
    Serial.print(data.calAltimeter);
    Serial.print(" changeDropLocation: ");
    Serial.print(data.changeDropLocation);
    Serial.print(" headwind: ");
    Serial.print(data.headwind);
    Serial.print(" manDrop: ");
    Serial.println(data.manDrop);
    Serial.print("newLat.degrees: ");
    Serial.print(data.newLat.degrees);
    Serial.print(" newLat.minutes: ");
    Serial.print(data.newLat.minutes, 6);
    Serial.print(" newLat.indicator: ");
    Serial.println(data.newLat.indicator);
    Serial.print("newLong.degrees: ");
    Serial.print(data.newLong.degrees);
    Serial.print(" newLong.minutes: ");
    Serial.print(data.newLong.minutes, 6);
    Serial.print(" newLong.indicator: ");
    Serial.println(data.newLong.indicator);
    Serial.print("packetsToDrop: ");
    Serial.print(data.packetsToDrop);
    Serial.print(" windspeed: ");
    Serial.println(data.windspeed, 2);
    Serial.println("------------------------------");
  }

  // Compare received with the expected
  if(expected.armed != data.armed)
  {
    Serial.println("Failed armed.");
    passed = 0;
  }
  else if(expected.manDrop != data.manDrop)
  {
    Serial.println("Failed manDrop.");
    passed = 0;
  }
  else if(expected.headwind != data.headwind)
  {
    Serial.println("Failed headwind.");
    passed = 0;
  }
  else if(expected.windspeed != data.windspeed)
  {
    Serial.println("Failed windspeed.");
    passed = 0;
  }
  else if(expected.packetsToDrop != data.packetsToDrop)
  {
    Serial.println("Failed packetsToDrop.");
    passed = 0;
  }
  else if(expected.changeDropLocation != data.changeDropLocation)
  {
    Serial.println("Failed changeDropLocation.");
    passed = 0;
  }
  else if(expected.newLong.degrees != data.newLong.degrees)
  {
    Serial.println("Failed long degrees.");
    passed = 0;
  }
  else if(expected.newLong.minutes != data.newLong.minutes)
  {
    Serial.println("Failed long minutes.");
    passed = 0;
  }
  else if(expected.newLong.indicator != data.newLong.indicator)
  {
    Serial.println("Failed long indicator.");
    passed = 0;
  }
  else if(expected.newLat.degrees != data.newLat.degrees)
  {
    Serial.println("Failed lat degrees.");
    passed = 0;
  }
  else if(expected.newLat.minutes != data.newLat.minutes)
  {
    Serial.println("Failed lat minutes.");
    passed = 0;
  }
  else if(expected.newLat.indicator != data.newLat.indicator)
  {
    Serial.println("Failed lat indicator.");
    passed = 0;
  }
  else if(expected.calAltimeter != data.calAltimeter)
  {
    Serial.println("Failed calAltimeter.");
    passed = 0;
  }

  Serial.println("Finished all.");
  
  return passed;
}

void constructPackets(transStruct* pBuf, int count)
{
  int i;
  transStruct c, p; // c = current packet; p = previous packet
  
  if(!pBuf)
  {
    return; // Null parameter
  }
  
  for(i = 1; i < count; i++)
  {
    c = pBuf[i];
    p = pBuf[i-1];

    // Latitude
    c.gpsData.latitude.degrees = p.gpsData.latitude.degrees;
    c.gpsData.latitude.minutes = p.gpsData.latitude.minutes * 1.00075;
    c.gpsData.latitude.indicator = p.gpsData.latitude.indicator;

    // Longitude
    c.gpsData.longitude.degrees = p.gpsData.longitude.degrees;
    c.gpsData.longitude.minutes = p.gpsData.longitude.minutes * 1.000025;
    c.gpsData.longitude.indicator = p.gpsData.longitude.indicator;
    
    c.gpsData.speed = p.gpsData.speed * 1.005;
    c.gpsData.heading = p.gpsData.heading * 0.995; // Decreasing heading
    c.haveDropped = i == count/2 ? 1 : 0; // At the halfway point, signal that a drop was made
    c.altitude = p.altitude * (1 + 0.01*pow(-1, i%3)); // Mimics a somewhat erratic behavior
    pBuf[i] = c;
  }
}

void comms_getData()
{
  char gpsStr[100];
	transStruct packet;
	packet.gpsData = gps_getData();
  packet.altitude = (float)Altimeter_getAvgAltitude_ft();
  //packet.altitude = 100.5;
  packet.header = 0x73;
//  packet.gpsData.latitude.degrees = 32;
//  packet.gpsData.latitude.minutes = 36.58326;
//  packet.gpsData.latitude.indicator = 0;
//  packet.gpsData.longitude.degrees = 97;
//  packet.gpsData.longitude.minutes = 29.01474;
//  packet.gpsData.longitude.indicator = 1;
//  packet.gpsData.speed = 32.42;
//  packet.gpsData.heading = 271.113;
  packet.haveDropped = 0;
//  packet.altitude = 104.67;

	comms_transmit(&packet);
//  Serial.print("validity: ");
//  Serial.print(packet.gpsData.validity);
//  Serial.print(" latitude degrees: ");
//  Serial.print(packet.gpsData.latitude.degrees);
//  Serial.print(" latitude minutes: ");
//  Serial.print(packet.gpsData.latitude.minutes,5);
//  Serial.print(" latitude indicator: ");
//  Serial.print(packet.gpsData.latitude.indicator);
//  Serial.print(" longitude degrees: ");
//  Serial.print(packet.gpsData.longitude.degrees);
//  Serial.print(" longitude minutes: ");
//  Serial.print(packet.gpsData.longitude.minutes,5);
//  Serial.print(" longitude indicator: ");
//  Serial.print(packet.gpsData.longitude.indicator);
//  Serial.print(" speed: ");
//  Serial.print(packet.gpsData.speed);
//  Serial.print(" heading: ");
//  Serial.print(packet.gpsData.heading);
//  Serial.print(" altitude: ");
//  Serial.println(packet.altitude);
}

//gps_data mock_gps_getData()
//{
//  gps_data data;
//  int i = 0;
//  char sentence[100];// = "$GPRMC,111636.932,A,02447.0949,N,12100.5223,E,000.0,000.0,030407,,,A*61\r\n";
//
//  memset(sentence, 0, 100);
//  
//  // Read in data from gps module
//  if(mockgpsSerial.available())
//  {
//    char ch = mockgpsSerial.read();
//    
//    while(ch != '$')
//    {
//      ch = mockgpsSerial.read();
//    }
//    
//    while (ch != '\n' && i < SENTENCESIZE)
//    {
//      if(ch != -1)
//      {
//        sentence[i] = ch;
//        i++;
//      }
//      
//      ch = mockgpsSerial.read();
//    }
//    sentence[i] = '\0';
//  }
//  else
//  {
//    Serial.println("GPS not available.");
//  }
//  
//  for(i = 0; i < 100; i++)
//  {
//    if(sentence[i] == 0)
//    {
//      break;
//    }
//    Serial.print(sentence[i]);
//  }
//  Serial.println("");
//  
//  // Parse the gps string into an easier to work with array
//  i = 0;
//  char* parsed[12];
//  parsed[i] = strtok(sentence, ",");
//  for (i = 0; parsed[i] != NULL; i++)
//  {
//    parsed[i+1] = strtok(NULL, ",");
//  }
//
//  // fill the gps_data struct to be returned
//  data.latitude.degrees = atoi(parsed[LATITUDE])/100;
//  data.latitude.minutes = fmod(atof(parsed[LATITUDE]),100);
//  if (strcmp(parsed[LATITUDE_HEMISPHERE], "N") == 0)
//    data.latitude.indicator = 0;
//  else if (strcmp(parsed[LATITUDE_HEMISPHERE], "S") == 0)
//    data.latitude.indicator = 1;
//  data.longitude.degrees = atoi(parsed[LONGITUDE])/100;
//  data.longitude.minutes = fmod(atof(parsed[LONGITUDE]),100);
//  if (strcmp(parsed[LONGITUDE_HEMISPHERE], "E") == 0)
//    data.longitude.indicator = 0;
//  else if (strcmp(parsed[LONGITUDE_HEMISPHERE], "W") == 0)
//    data.longitude.indicator = 1;
//  data.speed = atof(parsed[SPEED])*1.68781;  // constant is conversion of knots to feet per second
//  data.heading = atof(parsed[HEADING]);
//  if (strcmp(parsed[VALIDITY], "V") == 0)
//    data.validity = 0;
//  else if (strcmp(parsed[VALIDITY], "A") == 0)
//    data.validity = 1;
//
//
//  return data;
//}

