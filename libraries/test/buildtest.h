#ifndef BUILDTEST_H
#define BUILDTEST_H

typedef enum
{
  one = 1,
  two = 2,
  three = 3,
  four = 5
} test_val;

int test_function(char a);

#endif // Include guard