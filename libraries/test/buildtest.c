#include "buildtest.h"

// Return parameter as an "int" type through casting
int test_function(char a)
{
  return (int)a;
}