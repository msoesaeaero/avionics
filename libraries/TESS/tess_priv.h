//////////////////////////////////////////////////
// FILE: tess_priv.h                            //
//////////////////////////////////////////////////
// SUMMARY: This is the private header file for //
//   the TESS. Anything contained in here should//
//   only be known to direct components of the  //
//   TESS.                                      //
//////////////////////////////////////////////////

#ifndef TESS_PRIV_H
#define TESS_PRIV_H

#include <gpsData.h>
#include <math.h>
#include <Arduino.h>

#define TESS_AVAILABLE_PAYLOADS 0b00000011 // Two available payloads at start

#define TESS_DATA_BUFFER_LENGTH 4 // At 20Hz data rate => 1s of buffered data

// Meters per degree of latitude @ given latitude 
#define TESS_MPLAT(lat) (111132.92 - 559.82*cos(2*(lat)) +\
                         1.175*cos(4*(lat)) - 0.0023*cos(6*(lat)))

// Meters per degree of longitude @ given latitude
#define TESS_MPLON(lat) (111412.84*cos((lat)) -\
                         93.5*cos(3*(lat)) - 0.118*cos(5*(lat)))
                         
// Default threshold values in feet from target
#define TESS_HIGHER_THRESHOLD_DEFAULT 12
#define TESS_LOWER_THRESHOLD_DEFAULT 60

typedef struct gps_dataBuf
{
  int         length;
  int         isFull;
  int         index;
  gps_data*   data;
} gps_dataBuf;

typedef struct alt_dataBuf
{
  int   length;
  int   isFull;
  int   index;
  float*  data;
} alt_dataBuf;

typedef struct tess_dataBuf
{
  int     length;
  int     isFull;
  int     index;
  float*  data;
} tess_dataBuf;

typedef struct TESS
{
  gps_data      target;
  int           numPayloads;
  int           armed;
  int           lowerThreshold;
  int           higherThreshold;
  gps_dataBuf   gpsBuf;
  alt_dataBuf   elevationBuf;
  tess_dataBuf  predictBuf;
  int           availPayloads;
  int           dropMask;
  void*         pdas;
} TESS;


#endif