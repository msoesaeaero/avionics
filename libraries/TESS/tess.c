//////////////////////////////////////////////////
// FILENAME: tess.c                             //
//////////////////////////////////////////////////

#include "tess.h"
#include "tess_priv.h"
#include <das.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

// Altitude (ft)                100         103         106         109         112         115         118         121         // Speed (ft/s)
const float dragConst[112] = {  82.89458632,  83.60341958,	84.29168909,	84.97811096,	85.64496085,	86.30166226,	86.94852128,	87.58583058, // 35
                                86.15920631,  86.87841923,	87.57660666,	88.27276540,	88.94892988,	89.61466774,	90.27029828,	90.91612669, // 37
                                89.35445834,	90.08325694,	90.79060111,	91.49574735,	92.18050795,	92.85458410,	93.51830782,	94.17199637, // 39
                                92.48560671,	93.22328828,	93.93911641,	94.65258875,	95.34531234,	96.02711260,	96.69833383,	97.35930498, // 41
                                95.55734233,	96.30328288,	97.02699847,	97.74821149,	98.44833867,	99.13732146,	99.81551613,	100.4832630,  // 43
                                98.57386384,	99.32750777,	100.0585806,	100.7870150,	101.4940507,	102.1897379,	102.8744445,	103.5485223, // 45
                                101.5389448,	102.2997965,	103.0377546,	103.7729492,	104.4864547,	105.1884240,	105.8792367,	106.5592554, // 47
                                104.4559903,	105.2236067,	105.9680295,	106.7095744,	107.4291616,	108.1370405,	108.8336025,	109.5192215, // 49
                                107.3280835,	108.1020690,	108.8525816,	109.6001130,	110.3254383,	111.0388992,	111.7408986,	112.4318220,  // 51
                                110.1580267,	110.9380273,	111.6942962,	112.4474916,	113.1782524,	113.8970082,	114.6041739,	115.3001468, // 53
                                112.9483745,	113.7340745,	114.4958031,	115.2543777,	115.9903087,	116.7141097,	117.4262081,	118.1270135, // 55
                                115.7014634,	116.4925815,	117.2595075,	118.0232112,	118.7640810,	119.4927121,	120.2095448,	120.9150018, // 57
                                118.4194366,	119.2157233,	119.9876157,	120.7562301,	121.5018395,	122.2351181,	122.9565198,	123.6664812, // 59
                                121.1042656,	121.9055007,	122.6821576,	123.4554944,	124.2056742,	124.9434484,	125.6692854,	126.3836368 }; // 61

void tess_dropPayloads(TESS* ptess);

void tess_init(void* ptess)
{
  TESS* tess;
  
  if(!ptess)
  {
    return; // Null parameter
  }
  
  tess = (TESS*) ptess;
  
  // Free GPS coord buffer
  if(tess->gpsBuf.data)
  {
    free(tess->gpsBuf.data); // Free stored GPS data
  }
  
  // Free elevation buffer
  if(tess->elevationBuf.data)
  {
    free(tess->elevationBuf.data); // Free stored elevation data
  }
  
  // Free prediction buffer
  if(tess->predictBuf.data)
  {
    free(tess->predictBuf.data); // Free stored prediction data
  }
  
  memset(tess, 0, sizeof (TESS));
  
  // Allocate GPS coord buffer
  tess->gpsBuf.data = malloc(sizeof(gps_data) * TESS_DATA_BUFFER_LENGTH);
  
  // Allocate elevation buffer
  tess->elevationBuf.data = malloc(sizeof(float) * TESS_DATA_BUFFER_LENGTH);
  
  // Allocate prediction buffer
  tess->predictBuf.data = malloc(sizeof(float) * TESS_DATA_BUFFER_LENGTH);
  
  // Set available payloads to the default
  tess->availPayloads = TESS_AVAILABLE_PAYLOADS;
  
  // Set GPS data buffer properties to default
  tess->gpsBuf.length = TESS_DATA_BUFFER_LENGTH;
  tess->gpsBuf.isFull = 0;
  tess->gpsBuf.index  = 0;
  
  // Set Altimeter data buffer properties to default
  tess->elevationBuf.length = TESS_DATA_BUFFER_LENGTH;
  tess->elevationBuf.isFull = 0;
  tess->elevationBuf.index  = 0;
  
  // Set prediction data buffer properties to default
  tess->predictBuf.length = TESS_DATA_BUFFER_LENGTH;
  tess->predictBuf.isFull = 0;
  tess->predictBuf.index  = 0;
  
  // Initialize the number of available payloads to the default
  tess->availPayloads = TESS_AVAILABLE_PAYLOADS;
  
  // Initialize the threshold values
  tess->higherThreshold = TESS_HIGHER_THRESHOLD_DEFAULT;
  tess->lowerThreshold = TESS_LOWER_THRESHOLD_DEFAULT;
  
  // Reset the payload dropping masking bitfield
  tess->dropMask = 0;
}

void tess_setDAS(void* ptess, void* pdas)
{
  TESS* tess;
  
  if(!ptess || !pdas)
  {
    return; // Null parameter
  }
  
  tess = (TESS*)ptess;
  
  tess->pdas = pdas;
}

void tess_arm(void* ptess, gps_data targetLoc, int payloads)
{
  TESS* tess;
  
  if(!ptess)
  {
    return; // Null parameter
  }
  
  tess = (TESS*)ptess;
  
  if(tess->availPayloads == 0)
  {
    tess->armed = 0;
    return; // No available payloads remaining, do not arm
  }
  tess->target = targetLoc;
  tess->numPayloads = payloads;
  
  // Reset GPS data buffer
  tess->gpsBuf.isFull = 0;
  tess->gpsBuf.index = 0;
  
  // Reset Altimeter data buffer
  tess->elevationBuf.isFull = 0;
  tess->elevationBuf.index = 0;
  
  // Reset prediction data buffer
  tess->predictBuf.isFull = 0;
  tess->predictBuf.index = 0;
  
  tess->armed = 1;
}

void tess_disarm(void* ptess)
{
  TESS* tess;
  
  if(!ptess)
  {
    return; // Null parameter
  }
  
  // TODO: Clear the prediction buffer
  tess->armed = 0;
}

void tess_sendData(void* ptess, gps_data loc, int elevation)
{
  TESS* tess;
  
  if(!ptess)
  {
    return; // Null parameter
  }
  
  tess = (TESS*)ptess;
  
  if(loc.validity != 1)
  {
    return; // Invalid GPS data, do not save packet
  }
  
  // Add to GPS data buffer
  //
  //  Add data element at index, then increment index
  //memcpy(&(tess->gpsBuf.data[tess->gpsBuf.index++]), &loc, sizeof(gps_data));
  tess->gpsBuf.data[tess->gpsBuf.index++] = loc;
  //  Check for index out of range
  //    if EOB, reset index and set isFull
  if(tess->gpsBuf.index >= tess->gpsBuf.length)
  {
    tess->gpsBuf.index = 0;
    tess->gpsBuf.isFull = 1;
  }
  
  // Add to Altimeter data buffer
  //
  //  Add data element at index, then increment index
  tess->elevationBuf.data[tess->elevationBuf.index++] = elevation;
  //  Check for index out of range
  //    if EOB, reset index and set isFull
  if(tess->elevationBuf.index >= tess->elevationBuf.length)
  {
    tess->elevationBuf.index = 0;
    tess->elevationBuf.isFull = 1;
  }
}

// Latitude: z-axis
// Longitude: x-axis
float tess_predict(void* ptess)
{
  TESS*     tess;
  float     d;    // Estimated distance from position
  float     dx;   // Estimated distance - x axis
  float     dz;   // Estimated distance - z axis
  float     dgx;  // Estimated distance in degrees - x axis
  float     dgz;  // Estimated distance in degrees - z axis
  float     pgx;  // Longitude of current position
  float     pgz;  // Latitude of current position
  float     lgx;  // Longitude of landing location
  float     lgz;  // Latitude of landing location
  float     tgx;  // Longitude of target location
  float     tgz;  // Latitude of target location
  float     dist; // Distance from landing to target in feet
  gps_data  gps;  // Most recent GPS data
  float     elevation;      // Most recent elvation reading
  int       isDropped = 0;  // Boolean to indicate that prediction caused payload release
  
  if(!ptess)
  {
    return -1;
  }
  else
  {
    return -1;
  }
  tess = (TESS*)ptess;
  
  // Only predict if armed and data is available
  if(!(tess->armed) || (tess->elevationBuf.index == 0 && tess->elevationBuf.isFull == 0))
  {
    return -1; // Index is at beginning but we haven't wrapped around => empty
  }
  
  // Retrieve data from most recent GPS data
  if(tess->gpsBuf.index == 0)
  {
    gps = tess->gpsBuf.data[tess->gpsBuf.length-1];
  }
  else
  {
    gps = tess->gpsBuf.data[tess->gpsBuf.index-1];
  }
  
  // Retrieve data from most recent elevation reading
  if(tess->elevationBuf.index == 0)
  {
    elevation = tess->elevationBuf.data[tess->elevationBuf.length-1];
  }
  else
  {
    elevation = tess->elevationBuf.data[tess->elevationBuf.index-1];
  }
  
  // Convert position GPS to its decimal degrees format
  pgx = (gps.longitude.degrees + gps.longitude.minutes / 60.0) * pow(-1.0, gps.longitude.indicator);
  pgz = (gps.latitude.degrees + gps.latitude.minutes / 60.0) * pow(-1.0, gps.latitude.indicator);
  
  // Convert target GPS to its decimal degrees format
  tgx = (tess->target.latitude.degrees + tess->target.longitude.minutes / 60.0) * pow(-1.0, tess->target.longitude.indicator);
  tgz = (tess->target.latitude.degrees + tess->target.latitude.minutes / 60.0) * pow(-1.0, tess->target.latitude.indicator);
  
  // Estimated landing distance from current position
  d = dragConst[(int)(8 * (gps.speed + 1 - 35) / 2) + (int)((elevation + 1.5 - 100) / 3)] + gps.speed * 0.750; // Drag simulation distance plus .75 second lead time for wire warmup delay
  
  // x and z components of distance
  dx = d * sin(gps.heading * M_PI / 180.0);
  dz = d * cos(gps.heading * M_PI / 180.0);
  
  // Convert components to global coordinates degrees
  dgx = dx / 3.2808399 / TESS_MPLON(pgz);
  dgz = dz / 3.2808399 / TESS_MPLAT(pgz);
  
  // Calculate landing coordinate
  lgx = pgx + dgx;
  lgz = pgz + dgz;
  
  // Determine distance from landing to target (in feet)
  dist = 3.2808399 * sqrt(pow((tgx - lgx) * TESS_MPLON(((tgz+lgz)/2.0)), 2) + pow((tgz - lgz) * TESS_MPLAT(((tgz+lgz)/2.0)), 2));
  
  // Check to see if distance has broken higherThreshold.
  // If so, drop payload
  if(dist <= tess->higherThreshold)
  {
    // Drop
    isDropped = 1;
  }
  // If distance is between higher and lower thresholds, then check previous valid measurement.
  else if(dist <= tess->lowerThreshold)
  {
    // If distance is greater than prev meas, then drop payload.
    if(tess->predictBuf.index > 0)
    {
      if(dist > tess->predictBuf.data[tess->predictBuf.index - 1])
      {
        // Drop
        isDropped = 1;
      }
    }
    else if(tess->predictBuf.isFull && tess->predictBuf.index == 0)
    {
      if(dist > tess->predictBuf.data[tess->predictBuf.length - 1])
      {
        // Drop
        isDropped = 1;
      }
    }
  }
  
  // If a payload was dropped, send drop signal to connected DAS and enter disarmed mode.
  if(isDropped)
  {
    tess_dropPayloads(tess);
    das_signalDrop(tess->pdas);
    // Disarm
    tess_disarm(tess);
  }
  
  // Save distance to prediction buffer
  //
  //  Add data element at index, then increment index
  tess->predictBuf.data[tess->predictBuf.index++] = dist;
  //  Check for index out of range
  //    if EOB, reset index and set isFull
  if(tess->predictBuf.index >= tess->predictBuf.length)
  {
    tess->predictBuf.index = 0;
    tess->predictBuf.isFull = 1;
  }
  
  return tgx;//sqrt(pow(tgz - lgz, 2) + pow(tgx - lgx, 2));
}

void tess_dropNow(void* ptess)
{
  TESS* tess;
  
  if(!ptess)
  {
    return; // Null parameter
  }
  
  tess = (TESS*)ptess;
  
  tess_dropPayloads(tess);
  
  das_signalDrop(tess->pdas);
  
  // Disarm
  tess_disarm(tess);
}


/////////////////////////////////////////////////
// function: tess_dropPayloads                 //
/////////////////////////////////////////////////
// brief: Sets the dropMask bitfield to signal //
//    the system to drop the selected payloads.//
//    The payloads are selected using          //
//    numPayloads and availPayloads.           //
// parameters:                                 //
//    TESS* ptess Struct pointer to the TESS   //
//                  object that will have its  //
//                  payloads released.         //
// returns:                                    //
//    void                                     //
/////////////////////////////////////////////////
void tess_dropPayloads(TESS* ptess)
{
  int i;
  unsigned int j;
  
  if(!ptess)
  {
    return; // Null parameter
  }
  
  for(i = 0; i < ptess->numPayloads; i++)
  {
    if(ptess->availPayloads == 0) // No available payloads
    {
      break;
    }
    
    // Check availPayloads bitfield for a payload to drop
    for(j = 0; j < (sizeof ptess->availPayloads)*8; j++)
    {
      if(ptess->availPayloads & (1<<j)) // If the j'th bit is set, use that one
      {
        // Flag for drop
        ptess->dropMask += (1<<j);
        // Remove payload bit from available ones
        ptess->availPayloads &= ~(0b00000001<<j);
        break;
      }
    }
  }
}
