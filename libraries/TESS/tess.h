//////////////////////////////////////////////////
// FILE: tess.h									                //
//////////////////////////////////////////////////
// SUMMARY: This is the main header file for the//
//    TESS module. Various functions in here    //
//    will allow the features of the TESS to be //
//    accessed.                                 //
//////////////////////////////////////////////////

#ifndef TESS_H
#define TESS_H

#include <gpsData.h>

// Pins that are attached to accessible payloads
#define TESS_PAYLOADPIN_0       11
#define TESS_PAYLOADPIN_1       12

#define TESS_HOLDTIME           1500 // The amount of time to hold the
                                    // drop mechanism on in ms.

////
// Function: tess_init
//
// Brief: Initializes the properties of the ptess object
//
// Parameters: void* ptess - TESS struct pointer to initialize
//
// Returns: void
//
////
void tess_init(void* ptess);

////
// Function: tess_setDAS
//
// Brief: Sets the pdas property of the ptess object
//
// Parameters: void* ptess - TESS struct pointer
//             void* pdas  - DAS struct pointer to store in ptess
//
// Returns: void
//
////
void tess_setDAS(void* ptess, void* pdas);

////
// Function: tess_arm
//
// Brief: The ptess object is set to arm. Arming configuration is saved to 
//   the ptess object.
//
// Parameters: void* ptess          - TESS struct pointer to arm
//             gps_data* targetLoc - GPS location to target
//             int payloads         - Number of payloads to arm
//
// Returns: void
//
////
void tess_arm(void* ptess, gps_data targetLoc, int payloads);

////
// Function: tess_disarm
//
// Brief: The ptess object is set to disarm.
//
// Parameters: void* ptess - TESS struct pointer to disarm
//
// Returns: void
//
////
void tess_disarm(void* ptess);

////
// Function: tess_sendData
//
// Brief: Allows an outside source to send avionic data to the tess.
//
// Parameters: void* ptess    - TESS struct pointer to send the data to
//             gps_data* loc - GPS location data
//             int elevation  - Elevation data
//
// Returns: void
//
////
void tess_sendData(void* ptess, gps_data loc, int elevation);

////
// Function: tess_predict
//
// Brief: The ptess object predicts the current expected payload trajectory 
//   based on the data and configuration saved in the ptess object. The ptess 
//   object will drop the payload if the criterion are met.
//
// Parameters: void* ptess - TESS struct pointer to run the prediction
//                           calculations on
//
// Returns: void
//
////
float tess_predict(void* ptess);

////
// Function: tess_dropNow
//
// Brief: The ptess object drops one of its available payloads.
//
// Parameters: void* ptess - TESS struct pointer
//
// Returns: void
//
////
void tess_dropNow(void* ptess);

#endif