//////////////////////////////////////////////////
// FILENAME: comms.h                            //
//////////////////////////////////////////////////

#ifndef COMMS_H
#define COMMS_H

#include <gpsData.h>

typedef struct transStruct
{
  unsigned char header;
	gps_data gpsData;
	unsigned char haveDropped;
	float altitude;
} transStruct;

typedef struct recStruct
{
	unsigned char armed;//bool
	unsigned char manDrop;//bool
	unsigned char headwind;//bool
	float windspeed;
	int packetsToDrop;
	unsigned char changeDropLocation;//bool
	gps_longitude newLong;
	gps_latitude newLat;
	unsigned char calAltimeter;
} recStruct;

void comms_init();

void comms_transmit(const transStruct*);

int comms_receive(recStruct*);

#endif