//////////////////////////////////////////////////
// FILENAME: comms.cpp                            //
//////////////////////////////////////////////////

#include "comms.h"
#include <HardwareSerial.h>
#include <Arduino.h>

void comms_init()
{
    while(Serial.available() > 0)
    {
      char c = Serial.read();
    }
}

void comms_transmit(const transStruct* data)
{
  // Null parameter check
  if(!data)
  {
    return;
  }
  
  // Wait until avalable
	while(!Serial.availableForWrite());
	Serial.write((unsigned char*)data, sizeof(transStruct));
}

int comms_receive(recStruct* data)
{
	struct recStruct recData;//create pointer to data
	int dataSize = sizeof(recStruct);
	char input[dataSize];
  
  // Exit if null parameter or serial data is 
  //  not ready to be received
  if(!data || (unsigned)(Serial.available()) < sizeof(recStruct))
  {
    return 0;
  }
  
  // Read the receive data into the input stream
	Serial.readBytes(input, dataSize);
  
  // Unpack data buffer into a recStruct
	unsigned char* dataPointer = (unsigned char*) input;
	
	recData.armed = *(dataPointer);
  dataPointer++;
	recData.manDrop = *(dataPointer);
  dataPointer++;
	recData.headwind = *(dataPointer);
  dataPointer++;
	recData.windspeed = *((float*)dataPointer);
  dataPointer++;
  dataPointer++;
  dataPointer++;
  dataPointer++;
	recData.packetsToDrop = *((int*)dataPointer);
  dataPointer++;
  dataPointer++;
	recData.changeDropLocation = *((unsigned char*)dataPointer);
  dataPointer++;
	recData.newLong.degrees = *((unsigned char*)dataPointer);
  dataPointer++;
	recData.newLong.minutes = *((float*)dataPointer);
  dataPointer++;
  dataPointer++;
  dataPointer++;
  dataPointer++;
	recData.newLong.indicator = *((unsigned char*)dataPointer);
  dataPointer++;
	recData.newLat.degrees = *((unsigned char*)dataPointer);
  dataPointer++;
	recData.newLat.minutes = *((float*)dataPointer);
  dataPointer++;
  dataPointer++;
  dataPointer++;
  dataPointer++;
	recData.newLat.indicator = *((unsigned char*)dataPointer);
  dataPointer++;
	recData.calAltimeter = *((unsigned char*)dataPointer);
  
  *data = recData;
  
  comms_init();
  
  return 1;
}

