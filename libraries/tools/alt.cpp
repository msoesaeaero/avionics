//////////////////////////////////////////////////
// FILE: alt.c                                  //
//////////////////////////////////////////////////
// SUMMARY:                                     //
//////////////////////////////////////////////////

#include "alt.h"

#include <Wire.h>
#include <stdint.h>
#include <math.h>
#include <Arduino.h>

#define BASE_SAMPLES 10
#define PRESSURE_SAMPLES 12
#define TEMPERATURE_SAMPLES 2
#define ALTIMETER_ADDRESS 0x76
#define RESET 0x1E
#define ADC_READ 0x00
#define ADC_CONV 0x40
#define ADC_D1 0x00
#define ADC_D2 0x10
#define ADC_256 0x00
#define ADC_512 0x02
#define ADC_1024 0x04
#define ADC_2048 0x06
#define ADC_4096 0x08
#define PROM_READ 0xA0

#define SENS 0
#define OFF 1
#define TCS 2
#define TCO 3
#define TREF 4
#define TEMPSENS 5

const float P0 = 101570;
const float T0 = 288.15;
const float TEMP_GRADIENT = .0065;
const float R = 287.05;
const float g = 9.807;
uint16_t coefficients[6];
float baseHeight;

/*
Initialize the Altimeter and read in the static coefficients.  
Also find the base starting height of the altimeter
*/
void Altimeter_init()
{
	Altimeter_reset();
	Altimeter_readCoefficients();
  baseHeight = 0;
}

/*
Send a reset command to the altimeter
*/
void Altimeter_reset()
{
	Wire.begin();
	Wire.beginTransmission(ALTIMETER_ADDRESS);
	Wire.write(RESET);
	Wire.endTransmission();
	delayMicroseconds(3000);
}

/*
read in the reference coefficients from the gps into global coefficients array
*/
void Altimeter_readCoefficients()
{
	int i;
	for (i = 0; i < 6; i++)
	{
		uint16_t coefficient = 0;
		Wire.beginTransmission(ALTIMETER_ADDRESS);
		Wire.write(PROM_READ + (i + 1) * 2);
		Wire.endTransmission();

		Wire.requestFrom(ALTIMETER_ADDRESS, 2);
		if (Wire.available() >= 2)
		{
			coefficient = Wire.read();
			coefficient = coefficient << 8;
			coefficient = coefficient + Wire.read();
		}
		coefficients[i] = coefficient;
	}
}

/*
Starts a conversion on either the pressor or temperature sensor
sensor: using D1 or D2 to choose which sensor to start conversion
returns: the value of the sensor being read
*/
long Altimeter_adcRead(int sensor, int convSpeed)
{
	// tell the sensor to begin a conversion
	Wire.beginTransmission(ALTIMETER_ADDRESS);
	Wire.write(ADC_CONV | sensor | convSpeed);
	Wire.endTransmission();

	// ensure the conversion is finished before reading
	switch (convSpeed)
	{
	case ADC_256:
		delayMicroseconds(600);
		break;
	case ADC_512:
		delayMicroseconds(1170);
		break;
	case ADC_1024:
		delayMicroseconds(2280);
		break;
	case ADC_2048:
		delayMicroseconds(4540);
		break;
	case ADC_4096:
		delayMicroseconds(9040);
	}

	// send the read command to the sensor
	Wire.beginTransmission(ALTIMETER_ADDRESS);
	Wire.write(ADC_READ);
	Wire.endTransmission();

	// request data from the sensor and return the read value
	Wire.requestFrom(ALTIMETER_ADDRESS, 3);
	if (Wire.available() >= 3)
	{
		long value = Wire.read();
		value = value << 8;
		value = value + Wire.read();
		value = value << 8;
		value = value + Wire.read();

		return value;
	}

	return '\n';
}

/*
Calculates the converted pressure based on reference pressure and current temperature
returns: the compensated pressure in Pa
*/
long Altimeter_calculatePressure()
{
	// digital pressure value
	//unsigned long D1 = Altimeter_adcRead(ADC_D1 | ADC_4096);
	unsigned long D1 = Altimeter_avgPressure(ADC_2048);
	// digital temperature value
	//unsigned long D2 = Altimeter_adcRead(ADC_D2 | ADC_4096);
	unsigned long D2 = Altimeter_avgTemperature(ADC_2048);
  
	const long dT = D2 - ((unsigned long)coefficients[TREF] << 8);
	long TEMP = 2000 + (dT * (float)coefficients[TEMPSENS] / pow(2, 23));


	long offset = (unsigned long)coefficients[OFF] * 4 + (((float)coefficients[TCO] / 1024) * ((float)dT / 2048));
	long sensitivity = (unsigned long)coefficients[SENS] * 2 + (((float)coefficients[TCS] / 1024) * ((float)dT / 4096));

	// these equations won't work without changes because of the modified offset and sensitivity equations
	long T2;
	long OFF2;
	long SENS2;
	if (TEMP < 2000)
	{
		T2 = pow((float)dT, 2) / pow(2, 31);
		OFF2 = 61 * pow((float)TEMP - 2000, 2) / pow(2, 4);
		SENS2 = 2 * pow(TEMP - 2000, 2);
	}
	else
	{
		T2 = 0;
		OFF2 = 0;
		SENS2 = 0;
	}
	if (TEMP < -1500)
	{
		OFF2 = OFF2 + 15 * pow(TEMP + 1500, 2);
		SENS2 = SENS2 + 8 * pow(TEMP + 1500, 2);
	}

	TEMP -= (T2 / pow(2, 15));
	offset -= (OFF2 / pow(2, 15));
	sensitivity -= (SENS2 / pow(2, 15));

	long compensatedPressure = ((float)D1 / 2048) * ((float)sensitivity / 1024) - offset;

	return compensatedPressure;
}

/*
gets the altitude in feet
compensatedPressure: pressure being converted to height
return: altitude in feet
*/
float Altimeter_getAltitude_ft(long compensatedPressure)
{
	float altitude = (T0 / TEMP_GRADIENT) * (1 - pow((compensatedPressure / P0), (TEMP_GRADIENT*(R / g))));
	altitude = altitude * 3.28084;
	return altitude;
}

/*
find the average of multiple altitude readings based on the SAMPLE_SIZE
returns: a floating point value of the average altitude
*/
float Altimeter_getAvgAltitude_ft()
{
	float avgAltitude = 0;
	int i;
	for (i = 0; i < 1; i++)
	{
		avgAltitude += Altimeter_getAltitude_ft(Altimeter_calculatePressure());
	}
	return (avgAltitude / 1) - baseHeight;
}

/*
finds the base altitude upon startup and stores it in the baseHeight global
*/
void Altimeter_getStartingAltitude()
{
	baseHeight = 0;
	int i = 0;
	for (i = 0; i < BASE_SAMPLES; i++)
	{
		baseHeight += Altimeter_getAltitude_ft(Altimeter_calculatePressure());
	}

	baseHeight = baseHeight / BASE_SAMPLES;
}

unsigned long Altimeter_avgPressure(int convSpeed)
{
	unsigned long pressure = 0;
	int i = 0;
	for (i = 0; i < PRESSURE_SAMPLES; i++)
	{
		pressure += Altimeter_adcRead(ADC_D1, convSpeed);
	}

	return pressure / PRESSURE_SAMPLES;
}

unsigned long Altimeter_avgTemperature(int convSpeed)
{
	unsigned long temperature = 0;
	int i = 0;
  
	for (i = 0; i < TEMPERATURE_SAMPLES; i++)
	{
		temperature += Altimeter_adcRead(ADC_D2, convSpeed);
	}

	return temperature / TEMPERATURE_SAMPLES;
}
