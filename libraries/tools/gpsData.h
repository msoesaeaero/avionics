//////////////////////////////////////////////////
// FILE: gpsData.h                              //
//////////////////////////////////////////////////
// SUMMARY: This is the header file for the     //
//  tools related to the GPS's data.            //
//////////////////////////////////////////////////

#ifndef GPSDATA_H
#define GPSDATA_H

typedef struct gps_latitude
{
  unsigned char degrees;
  float         minutes;
  unsigned char indicator; // N = 0, S = 1
} gps_latitude;

typedef struct gps_longitude
{
  unsigned char degrees;
  float         minutes;
  unsigned char indicator;  // E = 0, W = 1
} gps_longitude;

typedef struct gps_data
{
  gps_latitude  latitude;
  gps_longitude longitude;
  float         speed;
  float         heading;
  unsigned char	validity;
} gps_data;

#endif
