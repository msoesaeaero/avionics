//////////////////////////////////////////////////
// FILE: alt.h                                  //
//////////////////////////////////////////////////
// SUMMARY: This is the header file for tools   //
//  for the altimeter.                          //
//////////////////////////////////////////////////

#ifndef ALT_H
#define ALT_H

#ifdef __cplusplus
extern "C"
{
#endif


void Altimeter_reset();
void Altimeter_readCoefficients();
long Altimeter_adcRead(int sensor);
long Altimeter_calculatePressure();
float Altimeter_getAltitude_ft(long compensatedPressure);
float Altimeter_getAvgAltitude_ft();
void Altimeter_getStartingAltitude();
void Altimeter_init();
unsigned long Altimeter_avgPressure(int convSpeed);
unsigned long Altimeter_avgTemperature(int convSpeed);

#ifdef __cplusplus
}
#endif

#endif