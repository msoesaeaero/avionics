//////////////////////////////////////////////////
// FILE: gps.h                                  //
//////////////////////////////////////////////////
// SUMMARY: This is the header file for the     //
//  tools related to the GPS.                   //
//////////////////////////////////////////////////

#ifndef GPS_H
#define GPS_H

#include <gpsData.h>

// Initializes the gps unit
void gps_init();

// returns a structure containing data from the gps
gps_data gps_getData();

#endif
