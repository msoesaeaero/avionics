//////////////////////////////////////////////////
// FILE: gps.c                                  //
//////////////////////////////////////////////////
// SUMMARY:                                     //
//////////////////////////////////////////////////

#include <gps.h>
#include <gpsData.h>
#include <Arduino.h>
#include <SoftwareSerial.h>
#include <stdlib.h>
#include <math.h>
#define SENTENCESIZE 72
#define TIME 1
#define VALIDITY 2
#define LATITUDE 3
#define LATITUDE_HEMISPHERE 4
#define LONGITUDE 5
#define LONGITUDE_HEMISPHERE 6
#define SPEED 7
#define HEADING 8
#define DATE 9
#define MAGNETIC_VARIATION 10
#define CHECKSUM 11

char sentence[SENTENCESIZE];

// GPS Software Serial Object (connect Tx of gps to 4 and Rx to 5
const byte Rx = 5;
const byte Tx = 4;
SoftwareSerial gpsSerial(Rx, Tx);

void gps_init()
{
	gpsSerial.begin(57600);
}

gps_data gps_getData() 
{
	gps_data data;
	int i = 0;
  const long timeout = 250;
  long startTime = 0;
  
  // Wait for serial port to become available
  startTime = millis();
  while(!gpsSerial.available())
  {
    if((millis() - startTime) > timeout)
    {
      data.validity = 0;
      return data; // GPS serial port took too long to become available
    }
  }

  char ch = gpsSerial.read();
  
  // Get the start of a GPS string
  while(ch == -1 || ch != '$')
  {
    ch = gpsSerial.read();
  }
  
  // Capture the GPS string
  while(ch != '\n' && ch != '\r' && i < SENTENCESIZE)
  {
    if(ch != -1) // Character not available in SoftwareSerial buffer
    {
      sentence[i] = ch;
      i++;
    }
    
    ch = gpsSerial.read(); // Get following character
  }
  
  sentence[i] = '\0';
  
	// Parse the gps string into an easier to work with array
	i = 0;
	char* parsed[13];
	parsed[i] = strtok(sentence, ",");
	for (i = 0; parsed[i] != NULL; i++)
	{
		parsed[i+1] = strtok(NULL, ",");
	}

	// fill the gps_data struct to be returned
	data.latitude.degrees = atoi(parsed[LATITUDE])/100;
	data.latitude.minutes = fmod(atof(parsed[LATITUDE]),100);
	if (strcmp(parsed[LATITUDE_HEMISPHERE], "N") == 0)
		data.latitude.indicator = 0;
	else if (strcmp(parsed[LATITUDE_HEMISPHERE], "S") == 0)
		data.latitude.indicator = 1;
	data.longitude.degrees = atoi(parsed[LONGITUDE])/100;
	data.longitude.minutes = fmod(atof(parsed[LONGITUDE]),100);
	if (strcmp(parsed[LONGITUDE_HEMISPHERE], "E") == 0)
		data.longitude.indicator = 0;
	else if (strcmp(parsed[LONGITUDE_HEMISPHERE], "W") == 0)
		data.longitude.indicator = 1;
	data.speed = atof(parsed[SPEED])*1.68781;  // constant is conversion of knots to feet per second
	data.heading = atof(parsed[HEADING]);
	if (strcmp(parsed[VALIDITY], "V") == 0)
		data.validity = 0;
	else if (strcmp(parsed[VALIDITY], "A") == 0)
		data.validity = 1;


	return data;
}
