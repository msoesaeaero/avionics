//////////////////////////////////////////////////
// FILENAME: das.h                              //
//////////////////////////////////////////////////

#ifndef DAS_H
#define DAS_H

#include <gpsData.h>

typedef struct das_data
{
  gps_data  gpsData;
  float       altimeterData;
} das_data;

typedef struct DAS
{
  das_data  dasData; // Data ready to send.
  int       dropped; // True if TESS has dropped.
} DAS;

#ifdef __cplusplus
extern "C"
{
#endif

das_data das_getData(void* pDAS);

void das_updateData(void* pDAS);

void das_signalDrop(void* pDAS);

void das_clearDrop(void* pDAS);

#ifdef __cplusplus
}
#endif

#endif
