//////////////////////////////////////////////////
// FILENAME: das.c                              //
//////////////////////////////////////////////////

#include <das.h>
#include <gps.h>
#include <alt.h>

das_data das_getData(void* pDAS)
{
  DAS* das;
  das_data dasData;
  
  if(!pDAS)
  {
    return dasData; // Null parameter
  }
  
  das = (DAS*)pDAS;
  // Get GPS Data

  // Get elevation data
  // Store data to DAS object
  das->dasData = dasData;
  return dasData;
}

void das_updateData(void* pDAS)
{
  DAS* das;
  
  if(!pDAS)
  {
    return;
  }
  
  das = (DAS*)pDAS;
  
  das->dasData.gpsData = gps_getData();
  das->dasData.altimeterData = Altimeter_getAvgAltitude_ft();
}

void das_signalDrop(void* pDAS)
{
  DAS* das;
  
  if(!pDAS)
  {
    return; // Null parameter
  }
  
  das = (DAS*)pDAS;
  
  das->dropped = 1;
}

void das_clearDrop(void* pDAS)
{
  DAS* das;
  
  if(!pDAS)
  {
    return; // Null parameter
  }
  
  das = (DAS*)pDAS;
  
  das->dropped = 0;
}
