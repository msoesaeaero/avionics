#include <das.h>
#include <comms.h>
#include <tess.h>

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.print("Starting ut_Sys.\n");
}

void loop() {
  // put your main code here, to run repeatedly:
  helloDAS(&Serial);
  helloComms(&Serial);
  helloTESS(&Serial);
  delay(1000);
}
