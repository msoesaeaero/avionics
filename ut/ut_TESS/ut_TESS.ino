extern "C"
{
  #include <tess.h>
  #include <tess_priv.h>
  #include <gps.h>
}

unsigned int elevationBuf[10] = {101, 102, 103, 101, 99, 96, 98, 100, 102, 110};
gps_data gpsBuf[10] = { { { 43, 2.5933330000, 0 }, { 87, 53.581666, 1 }, 30, 0 },
                        { { 43, 2.5933465135, 0 }, { 87, 53.581666, 1 }, 30, 0 },
                        { { 43, 2.5933600270, 0 }, { 87, 53.581666, 1 }, 30, 0 },
                        { { 43, 2.5933735405, 0 }, { 87, 53.581666, 1 }, 30, 0 },
                        { { 43, 2.5933870540, 0 }, { 87, 53.581666, 1 }, 30, 0 },
                        { { 43, 2.5934005675, 0 }, { 87, 53.581666, 1 }, 30, 0 },
                        { { 43, 2.5934140810, 0 }, { 87, 53.581666, 1 }, 30, 0 },
                        { { 43, 2.5934275945, 0 }, { 87, 53.581666, 1 }, 30, 0 },
                        { { 43, 2.5934411080, 0 }, { 87, 53.581666, 1 }, 30, 0 },
                        { { 43, 2.5934546215, 0 }, { 87, 53.581666, 1 }, 30, 0 } };

//struct mock_DAS
//{
//  
//}

void setup()
{
  TESS sTess;

  // tess_init test
  tess_init(&sTess);
  // Expected:
  //  The three data buffers are created, availPayloads is set to default (2),
  //  and overhead for the data buffers is set.
}

void loop()
{
  
}
