#include <SoftwareSerial.h>
#include <Wire.h>
#include <gps.h>
#include <alt.h>
#include <das.h>
#include <comms.h>

extern "C"
{
#include <tess.h>
#include <tess_priv.h>
}

/*
 * This is the main state machine for controlling the Data Acquisition System (DAS) and TESS.
 * 
 * Author: Aaron Rick, Kyle Kaja, Jeremy Dorn, and Jordan Downing
 * Version: 1.0
 * Date: 11/2/2015
 */

#define COMMS_TIMEOUT 5000 // 5000 ms = 5 seconds
#define PACKET_HEADER 0x73
#define TEST_PIN 13

enum state {START, NOTCON, FLIGHTU, FLIGHTA, BASEDISC} currentState; //These are the states of the plane

TESS tessModule; //Pointer to the tess module
DAS  dasModule; //Pointer to the das module
transStruct transmitData; //Data that will be transmitted
recStruct receiveData; //Data that is received
int _dropCounter; //Timer counter for delaying the wire cutter
unsigned long _commsWatchdog; //Watchdog timer for a communications timeout
char isHandshake; // True indicates that the open connection handshake has begun
char timer1ISR_Flag; // Flag indicates that the TIMER1 ISR has been called. We will perform actions outside of the ISR.

void TIMER1_action();
void checkCommsWatchdog();
void clearTransmitData();

void setup()
{
  Serial.begin(38400);

  // Set up the payload trigger pins to output pins
  pinMode(TESS_PAYLOADPIN_0, OUTPUT);
  pinMode(TESS_PAYLOADPIN_1, OUTPUT);
  digitalWrite(TESS_PAYLOADPIN_0, LOW);
  digitalWrite(TESS_PAYLOADPIN_1, LOW);

//  pinMode(TEST_PIN, OUTPUT);
//  digitalWrite(TEST_PIN, HIGH);
}

void loop()
{
  switch (currentState) {
    /*
     * State for the initial startup of the system. This state will be responsible for:
     * -Initializing the communication system
     * -Initializing the sensors
     * -Moves to the NOTCON state to begin verification of communication with the base station
     */
    case START:
      //call initialization of the communication module
      comms_init();
      isHandshake = 0;
      timer1ISR_Flag = 0;

      //initialize the tess
      tess_init(&tessModule);
      tess_setDAS(&tessModule, &dasModule);

      //initialize the das (actual component init done here?)
      Altimeter_init();
      gps_init();
      
      noInterrupts();

      //Initialize timer 1 for our 20 Hz signal rate
      TCCR1A = 0;
      TCCR1B = 0;
      TCNT1  = 0;
      
      OCR1A = 8000;            // compare register 16MHz/256/20Hz = 3125
      TCCR1B |= (1 << WGM12);   // enable CTC mode
      TIMSK1 |= (1 << OCIE1A);  // enable timer compare interrupt

      //Initialize timer 2 for the wire cutter delay
      _dropCounter = 0;
      TCCR2A = 0;
      TCCR2B = 0;
      TCNT2  = 0;
    
      TCCR2B |= (1 << WGM12);   // enable CTC mode
      TIMSK2 |= (1 << OCIE1A);  // enable timer compare interrupt
      
      interrupts();
      
      //state changes to NOTCON state
      currentState = NOTCON;
      _commsWatchdog = millis();
      break;
    /*
     * State for when the system is not connected to the base station. This state will be responsible for:
     * -Receiving a request from the PC base station to connect
     * -Acknowledges the request with an accept message
     * -Waits 20 seconds for an acknowledgement message from the PC base station.
     * -If acknowledge is received from the base station, state moves to FLIGHTU
     * -Remains in NOTCON if no acknowledge is received
     */
    case NOTCON:
      if(comms_receive(&receiveData)) // Connection established, enter flight mode
      {
        if(isHandshake == 1)
        {
          // Connection has been established. Enter flight mode.
          currentState = FLIGHTU;
//          digitalWrite(TEST_PIN, LOW);
          isHandshake = 0;
        }
        else
        {
          // Send first handshake and start to expect the second handshake from ground station
          isHandshake = 1;
          clearTransmitData();
          transmitData.header = PACKET_HEADER;
          comms_transmit(&transmitData);
        }
         _commsWatchdog = millis();
      }
      else if(isHandshake == 1)
      {
        checkCommsWatchdog(); // If we are expecting the second handshake but have not yet received it, check watchdog.
      }
      break;
    /*
     * State for when the system is recording flight data, but the payload is unarmed. This state will be responsible for:
     * -Reading the sensor data
     * -Creating a packet for transfer of data
     * -Transferring the data collected on sensors to the base station
     * -Receiving acknowledge bit and arming signal from base station
     * -If signal is acknowledged and signal is still unarmed, remain in FLIGHTU state
     * -If signal is acknowledged and signal is armed, state is moved to FLIGHTA
     */
    case FLIGHTU:
      // Enable 20 Hz data clock
      TCCR1B = 0;
      TCCR1B |= (1 << WGM12);   // enable CTC mode
      TCCR1B |= (1 << CS12);    // use the 256 clock prescaler

      // Receive data
      if(comms_receive(&receiveData))
      {
        _commsWatchdog = millis(); // Refresh comms watchdog

        // Calibrate the base height of the altimeter
        if(receiveData.calAltimeter == 1)
        {
          Altimeter_getStartingAltitude();
//          digitalWrite(TEST_PIN, HIGH);
        }
        
        //If arming signal is received, move to armed state
        if(!(tessModule.dropMask) && receiveData.armed == 1)
        {
          gps_data targetLoc;
          targetLoc.latitude = receiveData.newLat;
          targetLoc.longitude = receiveData.newLong;
          
          tess_arm(&tessModule, targetLoc, receiveData.packetsToDrop);
          if(tessModule.armed == 1)
          {
            currentState = FLIGHTA;
            _dropCounter = 0;       
          }
        }
      }
      else
      {
        checkCommsWatchdog();
      }
      break;
    /*
     * State for when the system is recording flight data and the payload is armed. This state will be responsible for:
     * -Reading the sensor data
     * -Data is shared with the TESS system which allows the payload to be dropped
     * -Creating a packet for transfer of data
     * -Transferring the data collected on sensors to the base station
     * -Receiving acknowledge bit and arming signal from base station
     * (Disarming is handled by comms system, as well as manual drop)
     * -If signal is acknowledged and signal is still armed, remain in FLIGHTA state
     * -If signal is acknowledge and signal is disarmed, state is moved to FLIGHTU
     */
    case FLIGHTA:
      // Enable 20 Hz data clock
      TCCR1B = 0;
      TCCR1B |= (1 << WGM12);   // enable CTC mode
      TCCR1B |= (1 << CS12);    // use the 256 clock prescaler
      
      if(tessModule.dropMask) // TESS ready to drop, start wire cutter delay timer
      {
        TCCR2B = 0;
        TCCR2B |= (1 << WGM12); // enable CTC mode
        TCCR2B |= (1<<CS12);    // Turn on timer 2 with the 256 prescaler
      }
      
      // Receive data
      if(comms_receive(&receiveData))
      {
        _commsWatchdog = millis(); // Refresh comms watchdog
        
        // Calibrate the base height of the altimeter
        if(receiveData.calAltimeter == 1)
        {
          Altimeter_getStartingAltitude();
//          digitalWrite(TEST_PIN, LOW);
        }

        //If we received a disarm signal, move back to unarmed state
        if(receiveData.armed == 0 && tessModule.armed == 1)
        {
          tess_disarm(&tessModule);
          currentState = FLIGHTU;
        }
        else if(receiveData.manDrop == 1)
        {
          tess_dropNow(&tessModule);
          if(tessModule.dropMask) // TESS ready to drop, start wire cutter delay timer
          {
            TCCR2B = 0;
            TCCR2B |= (1 << WGM12); // enable CTC mode
            TCCR2B |= (1<<CS12);    // Turn on timer 2 with the 256 prescaler
          }
          currentState = FLIGHTU;
        }
      }
      else
      {
        checkCommsWatchdog();
      }

      // If the TESS has disarmed, move to the unarmed state
      if(tessModule.armed == 0)
      {
        currentState = FLIGHTU;
      }
      break;
    /*
     * State for if the system becomes disconnected from the GUI during flight. This state will be responsible for: 
     * -Disarming the TESS if it is currently armed
     * -Ending flight recording
     * -The system then moves back into the NOTCON state
     */
    case BASEDISC:
      tess_disarm(&tessModule);
      TCCR1B &= !(1 << CS12); // Stop 20 Hz data timer
      currentState = NOTCON;
      break;
    /*
     * Default state for if the system somehow exits the state machine. It will restore proper operation by moving the system
     * back into the state machine at state START.
     */
    default:
      currentState = START;
      break;
  }
  
  if(timer1ISR_Flag == 1 && (currentState == FLIGHTU || currentState == FLIGHTA))
  {
    timer1ISR_Flag = 0;
    TIMER1_action();
  }
  else if(timer1ISR_Flag == 1)
  {
    timer1ISR_Flag = 0;
  }
}

void TIMER1_action()
{
  das_updateData(&dasModule);
  
  tess_sendData(&tessModule, dasModule.dasData.gpsData, dasModule.dasData.altimeterData);
  
//  if(currentState == FLIGHTA)
//  {
//    //Serial.println(tess_predict(&tessModule), 6);
//    tess_predict(&tessModule);
//  }

  clearTransmitData();
  transmitData.header = PACKET_HEADER;
  transmitData.gpsData = dasModule.dasData.gpsData;
  transmitData.haveDropped = dasModule.dropped;
  transmitData.altitude = dasModule.dasData.altimeterData;

  comms_transmit(&transmitData);
  
  das_clearDrop(&dasModule); // Clear drop flag since we only want to send it once.
}

//ISR for Timer1 cmp reg A, which determines our 20 Hz data rate.
//Data is sampled here from sensors.
//Data is sent to TESS
//If we are armed, the TESS predicts drop
//Pack and transmit data through comms
ISR(TIMER1_COMPA_vect)
{
  timer1ISR_Flag = 1;
}

//ISR for Timer2 cmp reg A, which determines the wire cutter delay
ISR(TIMER2_COMPA_vect)
{
  if(_dropCounter == 0)
  {
    if(tessModule.dropMask & 1)
    {
      digitalWrite(TESS_PAYLOADPIN_0, HIGH);
    }
    if(tessModule.dropMask & 2)
    {
      digitalWrite(TESS_PAYLOADPIN_1, HIGH);
    }
  }
  if(++_dropCounter >= TESS_HOLDTIME) // NiCr hold delay
  {
    if(tessModule.dropMask & 1)
    {
      digitalWrite(TESS_PAYLOADPIN_0, LOW);
    }
    if(tessModule.dropMask & 2)
    {
      digitalWrite(TESS_PAYLOADPIN_1, LOW);
    }
    TCCR2B = 0;    // Turn off timer
    _dropCounter = 0;
    tessModule.dropMask = 0;
  }
}

// Check to see if the comms watchdog has exceeded the max limit
void checkCommsWatchdog()
{
  long milS = millis() - _commsWatchdog;
  if(milS >= COMMS_TIMEOUT)
  {
    currentState = BASEDISC;
    isHandshake = 0;
  }
}

void clearTransmitData()
{
  gps_data tempGps;

  tempGps.latitude.degrees = 0;
  tempGps.latitude.minutes = 0.0;
  tempGps.latitude.indicator = 0;
  tempGps.longitude.degrees = 0;
  tempGps.longitude.minutes = 0.0;
  tempGps.longitude.indicator = 0;
  tempGps.speed = 0.0;
  tempGps.heading = 0.0;
  
  transmitData.header = 0;
  transmitData.gpsData = tempGps;
  transmitData.haveDropped = 0;
  transmitData.altitude = 0.0;
}

